const Utils = {

    getJwtToken: () => {
        return localStorage.getItem('token');
    },
    userRoles: {
        user: {
            role: 'user',
            displayName: 'User'
        },
        manager: {
            role: 'manager',
            displayName: 'Manager'
        },
        admin: {
            role: 'admin',
            displayName: 'Admin'
        }
    },
    getApiServerUrl: () => {
        const nodeEnv = process.env.NODE_ENV;

        if(nodeEnv === 'development') {
            return 'http://localhost:5000'
        } else if(nodeEnv === 'production') {
            return ''
        }

    },
    dailyReportEmailCsvFormatter: (data, fullData) => {

        return fullData.entries.map((entry) => {

            const fullEntry = data.find((dataEntry) => {
                return dataEntry.user.id === entry.user
            })

            if(fullEntry) {
                return [
                    fullEntry.user.name.firstname + ' ' + fullEntry.user.name.lastname,
                    fullEntry.hours,
                    fullEntry.location.name,
                    fullEntry.task.name,
                    fullEntry.notes
                ]
            } else {
                return [
                    entry.name.firstname + ' ' + entry.name.lastname,
                    entry.hours,
                    entry.location,
                    entry.task,
                    entry.notes
                ]
            }
        })

    }


}

export default Utils;