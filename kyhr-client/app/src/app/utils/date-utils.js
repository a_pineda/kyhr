const DateUtils = {
    dateToUTCString: (date, format) => {

        console.log(date.getTimezoneOffset())

        switch (format) {
            default:
                const weekday = DateUtils.weekIndexToString(date.getUTCDay(), 3);
                const month = DateUtils.monthIndexToString(date.getUTCMonth(), 3);
                const day = date.getUTCDate();
                const year = date.getUTCFullYear();

                return weekday + ' ' + month + ' ' + day + ' ' + year;
        }

    },
    dateToString: (date, format) => {
        switch (format) {
            default:
                const weekday = DateUtils.weekIndexToString(date.getDay(), 3);
                const month = DateUtils.monthIndexToString(date.getMonth(), 3);
                const day = date.getDate();
                const year = date.getFullYear();

                return weekday + ' ' + month + ' ' + day + ' ' + year;
        }
    },
    dateToLocaleString: (date, format, locale) => {

        const [month, day, year] = date.toLocaleDateString("en-US", {timeZone: locale}).split('/');
        const localDate = new Date(parseInt(year), parseInt(month) - 1, parseInt(day));

        switch (format) {
            default:
                const weekday = DateUtils.weekIndexToString(localDate.getDay(), 3);
                const month = DateUtils.monthIndexToString(localDate.getMonth(), 3);
                const day = localDate.getDate();
                const year = localDate.getFullYear();

                return weekday + ' ' + month + ' ' + day + ' ' + year;
        }
    },
    timeToString: (date, format) => {
        switch (format) {
            default:
                let hours = date.getHours();
                let minutes = date.getMinutes();
                let ampm = 'AM';

                if(minutes < 10) {
                    minutes = '0' + minutes;
                }

                if(hours > 12) {
                    hours -= 12;
                    ampm = 'PM';
                }

                return hours + ':' + minutes + ' ' + ampm;
        }
    },
    weekIndexToString: (index, numCharacters) => {
        return DateUtils.weekMap[index].substr(0, numCharacters);
    },
    monthIndexToString: (index, numCharacters) => {
        return DateUtils.monthMap[index].substr(0,numCharacters);
    },
    weekMap: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    monthMap: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']


}

export default DateUtils;