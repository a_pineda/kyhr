import { configureStore, getDefaultMiddleware, combineReducers } from '@reduxjs/toolkit';
import authReducer from './slices/auth-slice';
import timeInputReducer from './slices/time-input-slice';
import taskManagementReducer from './slices/task-management-slice';
import locationManagementReducer from './slices/location-management-slice';
import userManagementReducer from './slices/user-management-slice';
import errorPageReducer from './slices/error-slice';
import timeInputSubmissionHistoryReducer from './slices/time-input-submission-history-slice';


const middleware = [
  ...getDefaultMiddleware(),
  /*CUSTOM MIDDLEWARES HERE*/
];

export const store = configureStore({
  reducer: combineReducers({
    auth: authReducer,
    timeInput: timeInputReducer,
    taskManagement: taskManagementReducer,
    locationManagement: locationManagementReducer,
    userManagement: userManagementReducer,
    error: errorPageReducer,
    timeInputSubmissionHistory: timeInputSubmissionHistoryReducer
  }),
  middleware
});

export default store;
