import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import LocationApi from "../../api/locationApi";

const NAME = 'locationManagement';

export const getAllLocations = createAsyncThunk(
    `${NAME}/getAllLocationd`,
    async (companyId, thunkAPI) => {
        const response = await LocationApi.getAllLocationsForCompany(companyId)
        return response.data;
    })

export const updateLocation = createAsyncThunk(
    `${NAME}/updateLocation`,
    async (updatedLocation, thunkAPI) => {
        const response = await LocationApi.updateOne(updatedLocation)
        return response.data;
    })

export const addLocation = createAsyncThunk(
    `${NAME}/addLocation`,
    async (newLocation, thunkAPI) => {
        const response = await LocationApi.createLocation(newLocation)
        return response.data;
    })

const initialState = {
    locations: [],
    locationsDataFetchStatus: 'pending',
    updating: false
}

const locationManagementSlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        setSelectedLocation: (state, action) => {
            state.selectedLocation = action.payload;
        },
        setTempLocation: (state, action) => {
            state.tempLocation = action.payload;
        },
        setUpdating: (state, action) => {
            state.updating = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getAllLocations.pending, (state, action) => {
            state.locationsDataFetchStatus = 'pending';
        }).addCase(getAllLocations.fulfilled, (state, action) => {
            state.locationsDataFetchStatus = 'fulfilled';
            state.locations = action.payload.data.locations;
        }).addCase(getAllLocations.rejected, (state, action) => {
            state.locationsDataFetchStatus = 'rejected';
        }).addCase(updateLocation.pending, (state, action) => {
            state.submitStatus = 'pending';
        }).addCase(updateLocation.fulfilled, (state, action) => {
            state.submitStatus = 'fulfilled';
            const updatedLocation = action.payload.data.data;
            const index = state.locations.findIndex((location) => location.id === updatedLocation.id);
            state.locations[index] = updatedLocation;
            state.tempLocation = updatedLocation;
        }).addCase(updateLocation.rejected, (state, action) => {
            state.submitStatus = 'rejected';
        }).addCase(addLocation.pending, (state, action) => {
            state.submitStatus = 'pending';
        }).addCase(addLocation.fulfilled, (state, action) => {
            state.submitStatus = 'fulfilled';
            const newLocation = action.payload.data.data;
            state.locations.push(newLocation);
            state.selectedLocation = newLocation;
        }).addCase(addLocation.rejected, (state, action) => {
            state.submitStatus = 'rejected';
        })
    }
})

export const {setSelectedLocation, setTempLocation, setUpdating} = locationManagementSlice.actions

export default locationManagementSlice.reducer