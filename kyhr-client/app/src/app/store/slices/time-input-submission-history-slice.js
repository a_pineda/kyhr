import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import TimeInputSubmissionRecordApi from "../../api/timeInputSubmissionRecordApi";
import TimeEntryApi from "../../api/timeEntryApi";
import LocationApi from "../../api/locationApi";

const NAME = 'timeInputSubmissionHistory';

export const getAllTimeInputSubmissionRecords = createAsyncThunk(
    `${NAME}/getAllTimeInputSubmissionRecords`,
    async (companyId, thunkAPI) => {
        const response = await TimeInputSubmissionRecordApi.getAllTimeInputSubmissionRecordsForCompany(companyId)
        return response.data;
    })

export const getTimeEntryDataForRecord = createAsyncThunk(
    `${NAME}/getTimeEntryDataForRecord`,
    async (entryIds, thunkAPI) => {
        const response = await TimeEntryApi.getTimeEntryRecords(entryIds)
        return response.data;
    })

export const getAllLocations = createAsyncThunk(
    `${NAME}/getAllLocations`,
    async (companyId, thunkAPI) => {
        const response = await LocationApi.getAllLocationsForCompany(companyId)
        return response.data;
    })

const initialState = {
    timeInputSubmissionRecords: [],
    dataFetchStatus: 'pending',
    alertDismissed: false,
    fetchingEntriesForRecordStatus: 'pending',
    selectedRecordEntries: [],
    locations: []
}

const timeInputSubmissionHistorySlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        setAlertDismissed: (state, action) => {
            state.alertDismissed = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getAllTimeInputSubmissionRecords.pending, (state, action) => {
            state.dataFetchStatus = 'pending';
        }).addCase(getAllTimeInputSubmissionRecords.fulfilled, (state, action) => {
            state.dataFetchStatus = 'fulfilled';
            state.timeInputSubmissionRecords = action.payload.data.timeInputSubmissionRecords;
        }).addCase(getAllTimeInputSubmissionRecords.rejected, (state, action) => {
            state.dataFetchStatus = 'rejected';
        }).addCase(getTimeEntryDataForRecord.pending, (state, action) => {
            state.fetchingEntriesForRecordStatus = 'pending';
        }).addCase(getTimeEntryDataForRecord.fulfilled, (state, action) => {
            state.fetchingEntriesForRecordStatus = 'fulfilled';
            state.selectedRecordEntries = action.payload.data;
        }).addCase(getTimeEntryDataForRecord.rejected, (state, action) => {
            state.fetchingEntriesForRecordStatus = 'rejected';
        }).addCase(getAllLocations.pending, (state, action) => {
        }).addCase(getAllLocations.fulfilled, (state, action) => {
            state.locations = action.payload.data.locations;
        }).addCase(getAllLocations.rejected, (state, action) => {
        })
    }
})

export const {setAlertDismissed} = timeInputSubmissionHistorySlice.actions

export default timeInputSubmissionHistorySlice.reducer