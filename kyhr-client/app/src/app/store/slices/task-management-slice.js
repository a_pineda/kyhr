import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import TaskApi from "../../api/taskApi";

const NAME = 'taskManagement';

export const getAllTasks = createAsyncThunk(
    `${NAME}/getAllTasks`,
    async (companyId, thunkAPI) => {
        const response = await TaskApi.getAllTasksForCompany(companyId)
        return response.data;
    })

export const updateTask = createAsyncThunk(
    `${NAME}/updateTask`,
    async (updatedTask, thunkAPI) => {
        const response = await TaskApi.updateOne(updatedTask)
        return response.data;
    })

export const addTask = createAsyncThunk(
    `${NAME}/addTask`,
    async (newTask, thunkAPI) => {
        const response = await TaskApi.createTask(newTask)
        return response.data;
    })

const initialState = {
    tasks: [],
    tasksDataFetchStatus: 'pending',
    updating: false
}

const taskManagementSlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        setSelectedTask: (state, action) => {
            state.selectedTask = action.payload;
        },
        setTempTask: (state, action) => {
            state.tempTask = action.payload;
        },
        setUpdating: (state, action) => {
            state.updating = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getAllTasks.pending, (state, action) => {
            state.tasksDataFetchStatus = 'pending';
        }).addCase(getAllTasks.fulfilled, (state, action) => {
            state.tasksDataFetchStatus = 'fulfilled';
            state.tasks = action.payload.data.tasks;
        }).addCase(getAllTasks.rejected, (state, action) => {
            state.tasksDataFetchStatus = 'rejected';
        }).addCase(updateTask.pending, (state, action) => {
            state.submitStatus = 'pending';
        }).addCase(updateTask.fulfilled, (state, action) => {
            state.submitStatus = 'fulfilled';
            const updatedTask = action.payload.data.data;
            const index = state.tasks.findIndex((task) => task.id === updatedTask.id);
            state.tasks[index] = updatedTask;
        }).addCase(updateTask.rejected, (state, action) => {
            state.submitStatus = 'rejected';
        }).addCase(addTask.pending, (state, action) => {
            state.submitStatus = 'pending';
        }).addCase(addTask.fulfilled, (state, action) => {
            state.submitStatus = 'fulfilled';
            const newTask = action.payload.data.data;
            state.tasks.push(newTask);
            state.selectedTask = newTask;
        }).addCase(addTask.rejected, (state, action) => {
            state.submitStatus = 'rejected';
        })
    }
})

export const {setSelectedTask, setTempTask, setUpdating} = taskManagementSlice.actions

export default taskManagementSlice.reducer