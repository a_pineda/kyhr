import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import UserApi from "../../api/userApi";

const NAME = 'taskManagement';

export const getAllUsers = createAsyncThunk(
    `${NAME}/getAllUsers`,
    async (companyId, thunkAPI) => {
        const response = await UserApi.getAllUsersForCompany(companyId)
        return response.data;
    })

export const updateUser = createAsyncThunk(
    `${NAME}/updateUser`,
    async (updatedUser, thunkAPI) => {
        const response = await UserApi.updateUser(updatedUser)
        return response.data;
    })

export const updatePassword = createAsyncThunk(
    `${NAME}/updatePassword`,
    async (updatedPasswordItems, thunkAPI) => {
        const response = await UserApi.updatePassword(updatedPasswordItems)
        return response.data;
    })

export const addUser = createAsyncThunk(
    `${NAME}/addUser`,
    async (newUser, thunkAPI) => {
        const response = await UserApi.createNewUser(newUser)
        return response.data;
    })

const initialState = {
    users: [],
    usersDataFetchStatus: 'pending',
    updating: false,
    updatingPassword: false,
    submitStatus: ''
}

const userManagementSlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        setSelectedUser: (state, action) => {
            state.selectedUser = action.payload;
        },
        setTempUser: (state, action) => {
            state.tempUser = action.payload;
        },
        setUpdating: (state, action) => {
            state.updating = action.payload;
        },
        setUpdatingPassword: (state, action) => {
            state.updatingPassword = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getAllUsers.pending, (state, action) => {
            state.usersDataFetchStatus = 'pending';
        }).addCase(getAllUsers.fulfilled, (state, action) => {
            state.usersDataFetchStatus = 'fulfilled';
            state.users = action.payload.data.users;
        }).addCase(getAllUsers.rejected, (state, action) => {
            state.usersDataFetchStatus = 'rejected';
        }).addCase(updateUser.pending, (state, action) => {
            state.submitStatus = 'pending';
        }).addCase(updateUser.fulfilled, (state, action) => {
            state.submitStatus = 'fulfilled';
            const updatedUser = action.payload.data.user;
            const index = state.users.findIndex((user) => user.id === updatedUser.id);
            state.users[index] = updatedUser;
        }).addCase(updateUser.rejected, (state, action) => {
            state.submitStatus = 'rejected';
        }).addCase(addUser.pending, (state, action) => {
            state.submitStatus = 'pending';
        }).addCase(addUser.fulfilled, (state, action) => {
            state.submitStatus = 'fulfilled';
            const newUser = action.payload.data.data;
            state.users.push(newUser);
            state.selectedUser = newUser;
        }).addCase(addUser.rejected, (state, action) => {
            state.submitStatus = 'rejected';
        })
    }
})

export const {setSelectedUser, setTempUser, setUpdating, setUpdatingPassword} = userManagementSlice.actions

export default userManagementSlice.reducer