import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

import UserApi from '../../api/userApi';

const NAME = 'auth';

export const authenticate = createAsyncThunk(
    `${NAME}/authenticate`,
    async (credentials, thunkAPI) => {
        const response = await UserApi.authenticate(credentials)
        return response.data;
    })

export const logout = createAsyncThunk(
    `${NAME}/logout`,
    async (_, thunkAPI) => {
        const response = await UserApi.logout()
        return response.data;
    })

const initialState = {
    token: localStorage.getItem('token'),
    user: JSON.parse(localStorage.getItem('user')),
    submitState: ''
}

const authSlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        updateUser: (state, action) => {
            state.user = action.payload
            localStorage.setItem('user', JSON.stringify(action.payload));
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(authenticate.pending, (state) => {
                state.submitState = 'pending';
            })
            .addCase(authenticate.fulfilled, (state, action) => {
                state.submitState = 'fulfilled';
                state.token = action.payload.token;
                state.user = action.payload.data.user;
                localStorage.setItem('user', JSON.stringify(action.payload.data.user));
                localStorage.setItem('token', action.payload.token);
            })
            .addCase(authenticate.rejected, (state, action) => {
                state.submitState = 'rejected';
                state.errorMessage = 'Incorrect username or password'
            })
            .addCase(logout.pending, (state) => {

            })
            .addCase(logout.fulfilled, (state, action) => {
                state.submitState = '';
                state.token = '';
                state.user = '';
                localStorage.removeItem('user');
                localStorage.removeItem('token');
            })
            .addCase(logout.rejected, (state, action) => {

            })
    }
})

export const {updateUser} = authSlice.actions

export default authSlice.reducer