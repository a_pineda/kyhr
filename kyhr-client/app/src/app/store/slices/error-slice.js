import {createSlice} from "@reduxjs/toolkit";

const NAME = 'error';

const initialState = {
    statusCode: 418,
    errorMessage: 'An unknown error has occurred.'
}

const errorPageSlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        setStatusCode: (state, action) => {
            state.statusCode = action.payload;
        },
        setErrorMessage: (state, action) => {
            state.errorMessage = action.payload;
        }
    }
})

export const {setStatusCode, setErrorMessage} = errorPageSlice.actions

export default errorPageSlice.reducer