import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

import TimeEntryApi from "../../api/timeEntryApi";
import NodemailApi from "../../api/nodemailApi";
import Utils from "../../utils/utils";
import CompanyApi from "../../api/companyApi";
import UserApi from "../../api/userApi";
import LocationApi from "../../api/locationApi";
import TimeInputSubmissionRecordApi from "../../api/timeInputSubmissionRecordApi";

const NAME = 'timeInput';

export const submitDailyReport = createAsyncThunk(
    `${NAME}/submitDailyReport`,
    async (entryBatch, thunkAPI) => {
        const response = await TimeEntryApi.submitDailyReport(entryBatch.entries).then((res) => {

            const resData = res.data.data;

            thunkAPI.dispatch(setLastEntryBatch(thunkAPI.getState().timeInput.timeInputRows))

            const timeInputSubmissionRecord = {
                submissionDate: new Date(),
                forDate: thunkAPI.getState().timeInput.selectedDate,
                submittedBy: thunkAPI.getState().auth.user.id,
                company: thunkAPI.getState().auth.user.company,
                entries: res.data.data.map((entry) => entry.id),
                status: 'submitted'
            }

            if(timeInputSubmissionRecord.entries.length > 0) {
                thunkAPI.dispatch(createTimeInputSubmissionRecord(timeInputSubmissionRecord))
            } else {
                return Promise.reject()
            }

            return thunkAPI.dispatch(getReceivers()).then((res) => {
                const recipients = res.payload.data.doc.notificationEmailAddresses;
                const emailData = {
                    date: new Date(thunkAPI.getState().timeInput.selectedDate),
                    csvData: Utils.dailyReportEmailCsvFormatter(resData, thunkAPI.getState().timeInput.lastEntryBatch),
                    receivers: recipients
                }
                return thunkAPI.dispatch(sendEmail(emailData))
            })
        })
        return response.data;
    })

export const sendEmail = createAsyncThunk(
    `${NAME}/sendEmail`,
    async (emailData, thunkAPI) => {
        const response = await NodemailApi.sendDailyReportSubmissionNotification(emailData)
        return response.data;
    })

export const getReceivers = createAsyncThunk(
    `${NAME}/getReceivers`,
    async (emailData, thunkAPI) => {
        const response = await CompanyApi.getOneCompany(thunkAPI.getState().auth.user.company)
        return response.data;
    })

export const getAllUsers = createAsyncThunk(
    `${NAME}/getAllUsers`,
    async (companyId, thunkAPI) => {
        const response = await UserApi.getAllUsersForCompany(companyId)
        return response.data;
    })

export const getAllLocations = createAsyncThunk(
    `${NAME}/getAllLocations`,
    async (companyId, thunkAPI) => {
        const response = await LocationApi.getAllLocationsForCompany(companyId)
        return response.data;
    })

export const createTimeInputSubmissionRecord = createAsyncThunk(
    `${NAME}/createTimeInputSubmissionRecord`,
    async (timeInputSubmissionRecord, thunkAPI) => {
        const response = await TimeInputSubmissionRecordApi.createTimeInputSubmissionRecord(timeInputSubmissionRecord)
        return response.data;
    })

const initialState = {
    timeInputRows: [],
    selectedDate: new Date().setHours(12,0,0,0),
    lastEntryBatch: {
        date: new Date().setHours(12,0,0,0),
        entries: []
    },
    submissionStatus: 'not-submitted',
    users: [],
    entryIsValid: true
}

const authSlice = createSlice({
    name: NAME,
    initialState: initialState,
    reducers: {
        updateTimeInputRow: (state, action) => {
            console.log(action.payload);
            const index = state.timeInputRows.findIndex(row => row.user === action.payload.user);
            state.timeInputRows[index] = action.payload;
        },
        setTimeInputRows: (state, action) => {
            state.timeInputRows = action.payload;
        },
        updateSelectedDate: (state, action) => {
            const newDate = new Date(action.payload).setHours(12,0,0,0);
            state.selectedDate = newDate;
            state.timeInputRows.map((row) => {
                row.date = newDate;
                return newDate;
            })
        },
        setLastEntryBatch: (state, action) => {
            state.lastEntryBatch.entries = action.payload;
            state.lastEntryBatch.date = state.selectedDate;
        },
        setSubmittedFalse: (state, action) => {
            state.submissionStatus = 'not-submitted';
        },
        setEntryIsValid: (state, action) => {
            state.entryIsValid = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(submitDailyReport.pending, (state, action) => {
            state.submissionStatus = 'pending';
        }).addCase(submitDailyReport.fulfilled, (state, action) => {
            state.submissionStatus = 'submitted';
        }).addCase(submitDailyReport.rejected, (state, action) => {
            state.submissionStatus = 'rejected';
        }).addCase(getAllUsers.pending, (state, action) => {
            state.usersDataFetchStatus = 'pending';
        }).addCase(getAllUsers.fulfilled, (state, action) => {
            state.usersDataFetchStatus = 'fulfilled';
            state.users = action.payload.data.users;
        }).addCase(getAllUsers.rejected, (state, action) => {
            state.usersDataFetchStatus = 'rejected';
        }).addCase(getAllLocations.pending, (state, action) => {
            state.locationsDataFetchStatus = 'pending';
        }).addCase(getAllLocations.fulfilled, (state, action) => {
            state.locationsDataFetchStatus = 'fulfilled';
            state.locations = action.payload.data.locations;
        }).addCase(getAllLocations.rejected, (state, action) => {
            state.locationsDataFetchStatus = 'rejected';
        })
    }
})

export const {updateTimeInputRow, setTimeInputRows, updateSelectedDate, setLastEntryBatch, setSubmittedFalse, setEntryIsValid} = authSlice.actions

export default authSlice.reducer