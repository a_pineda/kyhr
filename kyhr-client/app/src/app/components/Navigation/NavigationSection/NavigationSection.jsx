import React from "react";

import * as classes from "./NavigationSection.module.scss";
import {useSelector} from "react-redux";

const NavigationSection = (props) => {

    const user = useSelector(state => state.auth.user);

    return (
        props.roles.includes(user.role) ?
        <div className={classes.NavigationSection}>
            <div className={classes.SectionDivider}></div>
            <div>{props.sectionTitle}</div>
                {props.children}
        </div>
            : <></>
    )

}

export default NavigationSection;