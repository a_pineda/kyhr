import React from 'react';
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import classes from './NavigationItem.module.scss';
import {NavItem} from "react-bootstrap";

/**
 * Individual Side Menu Navigation Item
 */
const NavigationItem = (props) => {
    return (
        <NavItem className={classes.NavigationItem} >
            <NavLink
                to={props.link}
                exact={props.exact}
                activeClassName={classes.active}>
                <FontAwesomeIcon className={classes.NavIcon} icon={props.icon}/>
                {props.children}
            </NavLink>
        </NavItem>
    );
}

NavigationItem.propTypes = {
    /** Path to render */
    link: PropTypes.string.isRequired,
    /** Exact */
    exact: PropTypes.bool,
    /** Font Awesome Icon */
    icon: PropTypes.object.isRequired,
    /** Text to be displayed */
    children: PropTypes.node.isRequired

}

NavigationItem.defaultProps = {
    exact: false
}

export default NavigationItem;