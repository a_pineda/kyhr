import React from 'react';
import {faChartBar, faUserClock, faMapMarkerAlt, faTasks, faBuilding, faUsers, faUser} from "@fortawesome/free-solid-svg-icons";
import {Nav, NavDropdown} from "react-bootstrap";

import NavigationItem from './NavigationItem/NavigationItem';
import Utils from "../../../utils/utils";

import classes from './NavigationItems.module.scss';
import NavigationSection from "../NavigationSection/NavigationSection";
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";

/**
 * Side Menu Navigation Items
 */
const NavigationItems = (props) => {

    const user = useSelector(state => state.auth.user);

    return (
        <>
            <Nav className={classes.MobileNav}>
                <Nav.Link href={'/dashboard'} as={NavLink} to={"/dashboard"}>Dashboard</Nav.Link>
                <Nav.Link href={'/myprofile'} as={NavLink} to={"/myprofile"}>My Profile</Nav.Link>
                <NavDropdown className={classes.DropdownMenu} id={'manager-section-dropdown'} title={'Manager'} hidden={user.role === 'user'}>
                    <NavDropdown.Item href={'/manager/timeinput'} as={NavLink} to={"/manager/timeinput"}>Time Input</NavDropdown.Item>
                    <NavDropdown.Item href={'/manager/locationmanagement'} as={NavLink} to={"/manager/locationmanagement"}>Locations</NavDropdown.Item>
                    <NavDropdown.Item href={'/manager/taskmanagement'} as={NavLink} to={"/manager/taskmanagement"}>Tasks</NavDropdown.Item>
                    <NavDropdown.Item href={'/manager/usermanagement'} as={NavLink} to={"/manager/usermanagement"}>Team</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown id={'admiun-section-dropdown'} title={'Admin'} hidden={user.role === 'user' || user.role === 'manager'}>
                    <NavDropdown.Item href={'/admin/companymanagement'} as={NavLink} to={"/admin/companymanagement"}>Company</NavDropdown.Item>
                </NavDropdown>
            </Nav>
            <Nav className={[classes.NavigationItems, 'flex-column']}>

                <NavigationSection roles={[Utils.userRoles.user.role, Utils.userRoles.manager.role, Utils.userRoles.admin.role]}>
                    <NavigationItem icon={faChartBar} link="/dashboard" exact>Dashboard</NavigationItem>
                    <NavigationItem icon={faUser} link="/myprofile" exact>My Profile</NavigationItem>
                </NavigationSection>

                <NavigationSection sectionTitle={Utils.userRoles.manager.displayName} roles={[Utils.userRoles.manager.role, Utils.userRoles.admin.role]}>
                    <NavigationItem icon={faUserClock} link="/manager/timeinput">Time Input</NavigationItem>
                    <NavigationItem icon={faMapMarkerAlt} link="/manager/locationmanagement">Locations</NavigationItem>
                    <NavigationItem icon={faTasks} link="/manager/taskmanagement">Tasks</NavigationItem>
                    <NavigationItem icon={faUsers} link="/manager/usermanagement">Team</NavigationItem>
                </NavigationSection>
                <NavigationSection sectionTitle={Utils.userRoles.admin.displayName} roles={[Utils.userRoles.admin.role]}>
                    <NavigationItem icon={faBuilding} link="/admin/companymanagement">Company</NavigationItem>
                </NavigationSection>

            </Nav>
        </>
    )
}

export default NavigationItems;