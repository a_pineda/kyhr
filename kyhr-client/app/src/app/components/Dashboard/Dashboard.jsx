import React from "react";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faUserClock, faMapMarkerAlt, faTasks, faUsers} from "@fortawesome/free-solid-svg-icons";

import * as classes from './Dashboard.module.scss';
import {Card, Col, Container, Row} from "react-bootstrap";
import {NavLink} from "react-router-dom";

const Dashboard = (props) => {

    return (
        <Container className={classes.Dashboard}>
            <Row>
                <Col md={6}>
                    <Card className={classes.DashboardCard} as={NavLink} to={'/manager/timeInput'}>
                        <Card.Body>
                            <h3>Time Input</h3>
                            <FontAwesomeIcon className={classes.DashboardCardIcon} icon={faUserClock}></FontAwesomeIcon>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={6}>
                    <Card className={classes.DashboardCard} as={NavLink} to={'/manager/locationManagement'}>
                        <Card.Body>
                            <h3>Locations</h3>
                            <FontAwesomeIcon className={classes.DashboardCardIcon} icon={faMapMarkerAlt}></FontAwesomeIcon>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={6}>
                    <Card className={classes.DashboardCard} as={NavLink} to={'/manager/taskManagement'}>
                        <Card.Body>
                            <h3>Tasks</h3>
                            <FontAwesomeIcon className={classes.DashboardCardIcon} icon={faTasks}></FontAwesomeIcon>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={6}>
                    <Card className={classes.DashboardCard} as={NavLink} to={'/manager/userManagement'}>
                        <Card.Body>
                            <h3>Team</h3>
                            <FontAwesomeIcon className={classes.DashboardCardIcon} icon={faUsers}></FontAwesomeIcon>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )

}

export default Dashboard;