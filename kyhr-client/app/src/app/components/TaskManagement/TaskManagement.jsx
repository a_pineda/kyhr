import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setSelectedTask, setTempTask, setUpdating, getAllTasks, updateTask, addTask} from "../../store/slices/task-management-slice";
import MasterListDetail from "../UI/MasterListDetail/MasterListDetail";
import AddUpdateView from "../UI/AddUpdateView/AddUpdateView";
import {Col, Form} from "react-bootstrap";

const TaskManagement = (props) => {

    const dispatch = useDispatch();

    const companyId = useSelector(state => state.auth.user.company);
    const tasks = useSelector(state => state.taskManagement.tasks);
    const selectedTask = useSelector(state => state.taskManagement.selectedTask);
    const tempTask = useSelector(state => state.taskManagement.tempTask);
    const submitStatus = useSelector(state => state.taskManagement.submitStatus);
    const updating = useSelector(state => state.taskManagement.updating);

    useEffect(() => {
        dispatch(getAllTasks(companyId));
    }, [dispatch, companyId])

    useEffect( () => {
        if(!updating) {
            dispatch(setTempTask({
                company: companyId,
                name: '',
                active: true
            }))
        }
    }, [dispatch, companyId, updating])

    const onAddUpdateClicked = () => {
        if(selectedTask) {
            dispatch(updateTask(tempTask))
        } else {
            dispatch(addTask(tempTask))
            dispatch(setUpdating(true));
        }
    }

    const inputChangedHandler = (event) => {
        switch (event.target.id) {
            case 'task-name':
                if(selectedTask) {
                    dispatch(setTempTask({
                        ...tempTask,
                        name: event.target.value
                    }))
                } else {
                    dispatch(setTempTask( {
                        company: companyId,
                        name: event.target.value
                    }))
                }
                break;
            case 'active-switch':
                dispatch(setTempTask({
                    ...tempTask,
                    active: event.target.checked
                }))
                break;
            default:
                break;
        }
    }

    const onListItemClicked = (task) => {
        dispatch(setSelectedTask({...task}));
        dispatch(setTempTask({...task}));
        dispatch(setUpdating(true));
    }

    const onAddButtonClicked = () => {
        dispatch(setSelectedTask(undefined));
        dispatch(setTempTask( {
            company: companyId,
            name: ''
        }))
        dispatch(setUpdating(false));
    }

    const addUpdateButtonDisabled = () => {
        return submitStatus === 'pending';
    }

    const displayNameFunction = (task) => {
        return task.name;
    }

    const taskForm = (
        <Form>
            <Form.Group>
                <Form.Row>
                    <Col md={6} lg={6} xl={6}>
                        <Form.Label>Task Name</Form.Label>
                        <Form.Control id={'task-name'} type={'text'} value={tempTask ? tempTask.name : ''} name={'task'} placeholder={'Task Name'} onChange={inputChangedHandler}/>
                    </Col>
                </Form.Row>
            </Form.Group>
            <Form.Row>
                <Col md={6} lg={6} xl={6}>
                    <Form.Label>Active Status</Form.Label>
                    <Form.Check id={'active-switch'} type={"switch"} label={"Active"} checked={tempTask ? tempTask.active : true}  onChange={inputChangedHandler}/>
                </Col>
            </Form.Row>
        </Form>
    )

    return (
            <MasterListDetail listTitle={'Tasks'} listItems={tasks} listItemClicked={onListItemClicked} addButton={true} addButtonClicked={onAddButtonClicked} displayName={displayNameFunction}>
                <AddUpdateView addUpdateButtonDisabled={addUpdateButtonDisabled()} onAddUpdateClicked={onAddUpdateClicked} updating={updating}>
                    <h3>Task Information</h3>
                    {taskForm}
                </AddUpdateView>
            </MasterListDetail>
    )
}

export default TaskManagement;