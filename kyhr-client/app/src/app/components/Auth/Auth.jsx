import React, {useEffect, useState} from "react";

import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import {Form, InputGroup, Button, Spinner} from "react-bootstrap";

import * as actions from '../../store/slices/auth-slice';

import * as classes from './Auth.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


const Auth = (props) => {

    const dispatch = useDispatch();

    const submitState = useSelector(state => state.auth.submitState);
    const errorMessage = useSelector(state => state.auth.errorMessage);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        if(submitState === 'rejected') {
            setValidated(false);
        } else if(submitState === 'fulfilled') {
            setValidated(true);
        }
    }, [submitState])

    const emailChangedHandler = (event) => {
        setEmail(event.target.value);
    }

    const passwordChangedHandler = (event) => {
        setPassword(event.target.value);
    }

    const loginHandler = (event) => {
        event.preventDefault();
        const form = event.currentTarget;

        if(form.checkValidity() === false) {
            event.stopPropagation();
        } else {
            dispatch(actions.authenticate({email: email, password: password}));
        }

        setValidated(true);
    }

    return (
        <div className={classes.Backdrop}>
            <div className={classes.Auth}>
                <h2>KYHR Login</h2>
                <Form noValidate onSubmit={loginHandler} validated={validated}>
                    <Form.Group>
                        <InputGroup hasValidation>
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon={faUser}/>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                required
                                type={"text"}
                                placeholder={"Username or Email"}
                                value={email}
                                onChange={emailChangedHandler} />
                            <Form.Control.Feedback type="invalid">
                                Username or Email is required.
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>

                    <Form.Group>
                        <InputGroup hasValidation>
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon={faLock}/>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                required
                                type={"password"}
                                placeholder={"Password"}
                                value={password}
                                onChange={passwordChangedHandler} />
                            <Form.Control.Feedback type="invalid">
                                Password is required.
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>
                    {submitState === 'rejected' ? <div className={classes.Error}>{errorMessage}</div> : <></>}
                    {submitState !== 'pending' ? <Button variant="primary" type="submit">
                        Submit
                    </Button> : <Spinner animation={'border'} variant={'primary'}/>}



                </Form>
            </div>
        </div>
    );

}

export default Auth;