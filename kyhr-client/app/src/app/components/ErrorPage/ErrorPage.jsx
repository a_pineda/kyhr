import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {Jumbotron, Button} from "react-bootstrap";
import {setErrorMessage, setStatusCode} from "../../store/slices/error-slice";

const ErrorPage = (props) => {

    const dispatch = useDispatch();

    if(props.statusCode) {
        dispatch(setStatusCode(props.statusCode))
    }

    if(props.errorMessage) {
        dispatch(setErrorMessage(props.errorMessage))
    }

    const statusCode = useSelector(state => state.error.statusCode);
    const errorMessage = useSelector(state => state.error.errorMessage);

    return (
        <Jumbotron>
            <h1>{statusCode}</h1>
            <p>
                {errorMessage}
            </p>
            <p>
                <Button variant="primary" as={NavLink} to={'/dashboard'}>Return Home</Button>
            </p>
        </Jumbotron>
    )
}

export default ErrorPage;