import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {setErrorMessage, setStatusCode} from "../../../store/slices/error-slice";

/**
 * Wrapper for Route that allows for auth protected views
 */
const AuthRoute = (props) => {

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.auth.user);

    if(!props.roles.includes(currentUser.role)) {

        dispatch(setStatusCode(403))
        dispatch(setErrorMessage('You are not authorized to access this content. Please contact your system administrator if you believe this is an error.'))

        return <Redirect to='/error'/>;
    }

    return (
        <Route {...props} />
    )

};

export default AuthRoute;
