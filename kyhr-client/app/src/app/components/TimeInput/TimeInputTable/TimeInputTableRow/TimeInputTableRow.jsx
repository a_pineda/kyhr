import React, {useEffect, useState} from "react";

import * as classes from './TimeInputTableRow.module.scss';
import {FormControl, InputGroup} from "react-bootstrap";
import Select from "react-select";
import {useDispatch} from "react-redux";
import {updateTimeInputRow} from "../../../../store/slices/time-input-slice";

const TimeInputTableRow = (props) => {

    const locations = props.locations;

    const dispatch = useDispatch()

    const [selectedLocation, setSelectedLocation] = useState();
    const [selectedLocationOption, setSelectedLocationOption] = useState();
    const [selectedTask, setSelectedTask] = useState();
    const [selectedTaskOption, setSelectedTaskOption] = useState();

    useEffect(() => {
        if (props.rowData.location) {
            setSelectedLocationOption(locationsToOptions.find((loc) => {
                return loc.value === props.rowData.location;
            }))
            setSelectedLocation(locations.find((loc) => {
                return loc.id === props.rowData.location;
            }))
        }
    }, [])

    useEffect(() => {
        if (props.rowData.task && selectedLocation && !selectedTask) {
            setSelectedTaskOption(tasksToOptions.find((task) => {
                return task.value === props.rowData.task;
            }))
        }
    }, [selectedLocation])

    const inputChangedHandler = (event) => {
        switch (event.target.id) {
            case 'hours-input':
                props.hoursChangedHandler(props.rowData, event.target.value)
                break;
            case 'notes-input':
                props.notesChangedHandler(props.rowData, event.target.value)
                break;
            default:
                break;
        }
    }

    const locationSelectChangedHandler = (event) => {
        setSelectedLocation(locations.find((location) => {
            return location.id === event.value;
        }))
        setSelectedLocationOption(locationsToOptions.find((loc) => {
            return loc.value === event.value;
        }))
        props.locationChangedHandler(props.rowData, event.value)
    }

    const taskSelectChangedHandler = (event) => {
        setSelectedTaskOption(tasksToOptions.find((task) => {
            return task.value === event.value;
        }))
        setSelectedTask(selectedLocation.tasks.find((task) => {
            return task.id === event.value;
        }))
        props.taskChangedHandler(props.rowData, event.value)
    }

    const locationsToOptions = locations ? locations.map((location) => {
        return {label: location.name, value: location.id}
    }) : []

    const tasksToOptions = selectedLocation ? selectedLocation.populatedTasks.map((task) => {
        return {label: task.name, value: task.id}
    }) : []

    const hoursInput = (
        <InputGroup>
            <FormControl id={'hours-input'} value={props.rowData.hours} placeholder={0.0} type={'number'}
                         onChange={inputChangedHandler}/>
        </InputGroup>
    )

    const locationSelect = (
        <Select styles={{
            control: styles => ({
                ...styles,
                borderColor: props.rowData.hours > 0 && !selectedLocationOption ? 'red' : styles.borderColor,
                '&:hover': {
                    borderColor: props.rowData.hours > 0 && !selectedLocationOption ? 'red' : styles['&:hover'].borderColor,
                }
            })
        }} isSearchable options={locationsToOptions} value={selectedLocationOption}
                onChange={locationSelectChangedHandler}/>
    )

    const taskSelect = (
        <Select styles={{
            control: styles => ({
                ...styles,
                borderColor: selectedLocationOption && !selectedTaskOption ? 'red' : styles.borderColor,
                '&:hover': {
                    borderColor: selectedLocationOption && !selectedTaskOption ? 'red' : styles['&:hover'].borderColor,
                }
            })
        }} isSearchable options={tasksToOptions} value={selectedTaskOption} isDisabled={!selectedLocation}
                onChange={taskSelectChangedHandler}/>
    )

    const notesInput = (
        <InputGroup>
            <FormControl id={'notes-input'} as={'textarea'} value={props.rowData.notes} onChange={inputChangedHandler}/>
        </InputGroup>
    )

    return (
        <tr className={[classes.TimeInputTableRow, 'd-flex']}>
            <td className={classes.TableData} style={{width: '15%'}}>{props.rowData.name.firstname + ' ' + props.rowData.name.lastname}</td>
            <td className={classes.TableData} style={{width: '10%'}}>{hoursInput}</td>
            <td className={classes.TableData} style={{width: '20%'}}>{locationSelect}</td>
            <td className={classes.TableData} style={{width: '20%'}}>{taskSelect}</td>
            <td className={classes.TableData} style={{width: '20%'}}>{notesInput}</td>
        </tr>
    )
}

export default TimeInputTableRow;