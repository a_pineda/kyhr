
export const rowInputElementConfig = (data, locationOptions, taskOptions) => {
    return {
        hours: {
            elementType: 'input',
            elementConfig: {
                type: 'number',
                placeholder: 'hrs',
                min: 0,
                max: 24,
                step: 1
            },
            value: parseFloat(data.hours).toFixed(2),
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        location: {
            elementType: 'select',
            elementConfig: {
                options: [
                    {
                        value: data.location,
                        displayValue: 'Select a Location',
                        disabled: true
                    },
                    ...locationOptions]
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        task: {
            elementType: 'select',
            elementConfig: {
                options: [
                    {
                        value: data.task,
                        displayValue: 'Select a Task',
                        disabled: true
                    },
                    ...taskOptions]
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        notes: {
            elementType: 'textarea',
            elementConfig: {},
            value: data.notes,
            validation: {
                required: true,
                minLength: 6
            },
            valid: false,
            touched: false
        }
    }
}