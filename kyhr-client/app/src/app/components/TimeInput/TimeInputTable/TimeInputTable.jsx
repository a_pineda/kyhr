import React, {useEffect} from "react";

import * as classes from './TimeInputTable.module.scss';
import {useDispatch, useSelector} from "react-redux";
import TimeInputTableRow from "./TimeInputTableRow/TimeInputTableRow";
import {setEntryIsValid, setTimeInputRows, updateTimeInputRow} from "../../../store/slices/time-input-slice";
import {Table} from "react-bootstrap";

const TimeInputTable = (props) => {

    const timeInputRows = useSelector(state => state.timeInput.timeInputRows);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setTimeInputRows(buildTimeInputRows(props.users)))
    }, [dispatch, props.users])

    useEffect(() => {
        dispatch(setEntryIsValid(true))
        timeInputRows.map((row) => {
            if(!validateRow(row)) {
                dispatch(setEntryIsValid(false));
                return;
            }
        })
    }, [timeInputRows])

    const validateRow = (row) => {
        if(row.hours > 0 && (!row.location || !row.task)) {
            return false;
        } else if(row.location && !row.task) {
            return false;
        } else {
            return true;
        }
    }

    const buildTimeInputRows = (usersArray) => {
        const rows = usersArray.filter((user) => {
            if(!user.active) {
                return false;
            }
            return true;
        }).map((user) => {
            let existingRecord = timeInputRows.find((row) => row.user === user.id);
            return {
                user: user.id,
                name: user.name,
                hours: existingRecord ? existingRecord.hours : 0,
                location: existingRecord ? existingRecord.location : '',
                task: existingRecord ? existingRecord.task : '',
                notes: existingRecord ? existingRecord.notes : '',
                date: existingRecord ? existingRecord.date : Date.now()
            }
        })
        return rows;
    }

    const valueChangedHandler = (valueKey, existingRowData, value) => {
        switch (valueKey) {
            case 'hours':
                dispatch(updateTimeInputRow({...existingRowData, hours: value}))
                break;
            case 'location':
                dispatch(updateTimeInputRow({...existingRowData, location: value}))
                break
            case 'task':
                dispatch(updateTimeInputRow({...existingRowData, task: value}))
                break;
            case 'notes':
                dispatch(updateTimeInputRow({...existingRowData, notes: value}))
                break;
        }
    }

    const rows = timeInputRows.map((userRow) => (
            <TimeInputTableRow
                key={userRow.user}
                rowData={userRow}
                locations={props.locations}
                hoursChangedHandler={(existingData, value) => valueChangedHandler('hours', existingData, value)}
                locationChangedHandler={(existingData, value) => valueChangedHandler('location', existingData, value)}
                taskChangedHandler={(existingData, value) => valueChangedHandler('task', existingData, value)}
                notesChangedHandler={(existingData, value) => valueChangedHandler('notes', existingData, value)}
            />
        )
    )

    return (
        <Table striped bordered size={'sm'} className={classes.TimeInputTable}>
            <thead>
                <tr className={'text-center'}>
                    <th>Name</th>
                    <th>Hours</th>
                    <th>Location</th>
                    <th>Task</th>
                    <th>Notes</th>
                </tr>
            </thead>

            <tbody>
            {rows}
            </tbody>
        </Table>
    )
}

export default TimeInputTable;