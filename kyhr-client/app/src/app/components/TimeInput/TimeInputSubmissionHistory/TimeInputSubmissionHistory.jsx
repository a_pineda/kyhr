import React, {useEffect, useState} from "react";

import {Badge, ListGroup, Col, Row, Alert, Accordion, Card} from "react-bootstrap";

import * as classes from './TimeInputSubmissionHistory.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {
    getAllTimeInputSubmissionRecords,
    getTimeEntryDataForRecord,
    setAlertDismissed
} from "../../../store/slices/time-input-submission-history-slice";
import DateUtils from "../../../utils/date-utils";
import LoadingIcon from "../../UI/LoadingIcon/LoadingIcon";
import SubmissionEntryTable from "./SubmissionEntryTable";

const TimeInputSubmissionHistory = (props) => {

    const dispatch = useDispatch();

    const companyId = useSelector(state => state.auth.user.company);
    const currentUser = useSelector(state => state.auth.user);
    const timeInputSubmissionRecords = useSelector(state => state.timeInputSubmissionHistory.timeInputSubmissionRecords);
    const alertDismissed = useSelector(state => state.timeInputSubmissionHistory.alertDismissed);
    const selectedRecordEntries = useSelector(state => state.timeInputSubmissionHistory.selectedRecordEntries);
    const fetchingEntriesForRecordStatus = useSelector(state => state.timeInputSubmissionHistory.fetchingEntriesForRecordStatus);

    const [selectedEventKey, setSelectedEventKey] = useState();


    useEffect(() => {
        dispatch(getAllTimeInputSubmissionRecords(companyId));
    }, [dispatch, companyId])

    const populateEntries = (entryIds, newEventKey, updated) => {
        if(currentUser.role === 'admin' && (selectedEventKey !== newEventKey || updated)) {
            setSelectedEventKey(newEventKey)
            dispatch(getTimeEntryDataForRecord(entryIds))
        }
    }

    return (
        <div className={classes.TimeInputSubmissionHistory}>
            <Alert variant="warning" hidden={alertDismissed} onClose={() => dispatch(setAlertDismissed(true))} dismissible>
                <p>
                    "For Date" is shown in America/Cayman Timezone (GMT +5). All other dates and times are in your local timezone.
                </p>
            </Alert>
            <Accordion >
                {timeInputSubmissionRecords.map((record, index) => {
                    return (
                        <Card key={record.id}>
                            <Accordion.Toggle as={Card.Header} eventKey={index + 1} onClick={() => populateEntries(record.entries, index, false)}>
                            <Row>
                                <Col md={2}>
                                    <div className={classes.ListItemDataHeader}>Submission Date</div>
                                    <Badge pill variant={'light'}>{new Date(record.submissionDate).toDateString()}</Badge>
                                </Col>
                                <Col md={2}>
                                    <div className={classes.ListItemDataHeader}>Submission Time</div>
                                    <Badge pill variant={'light'}>{DateUtils.timeToString(new Date(record.submissionDate))}</Badge>
                                </Col>
                                <Col md={2}>
                                    <div className={classes.ListItemDataHeader}>For Date</div>
                                    <Badge pill variant={'light'}>{DateUtils.dateToLocaleString(new Date(record.forDate), null,"America/Cayman")}</Badge>
                                </Col>
                                <Col md={2}>
                                    <div className={classes.ListItemDataHeader}>Submitted By</div>
                                    <Badge pill variant={'primary'}>{record.submittedBy.name.firstname + ' ' + record.submittedBy.name.lastname}</Badge>
                                </Col>
                                <Col md={2}>
                                    <div className={classes.ListItemDataHeader}># Of Entries</div>
                                    <Badge pill variant={'light'}>{record.entries.length}</Badge>
                                </Col>
                                <Col md={2}>
                                    <div className={classes.ListItemDataHeader}>Status</div>
                                    <Badge pill variant={record.status === 'submitted' ? 'success' : 'danger'}>{record.status === 'submitted' ? 'Submitted' : 'Error'}</Badge>
                                </Col>
                            </Row>
                            </Accordion.Toggle>
                            {currentUser.role === 'admin' ?
                            <Accordion.Collapse eventKey={index + 1}>
                                <Card.Body>
                                    {fetchingEntriesForRecordStatus === 'pending' ? <></> : <SubmissionEntryTable entries={selectedRecordEntries} onEntryUpdated={() => populateEntries(record.entries, index, true)}/>}
                                </Card.Body>
                            </Accordion.Collapse> : <></>}
                        </Card>
                    )
                })}

            </Accordion>
        </div>
    )

}

export default TimeInputSubmissionHistory;