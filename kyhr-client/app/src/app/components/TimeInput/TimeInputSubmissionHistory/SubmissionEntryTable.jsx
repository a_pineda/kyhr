import React, {useEffect, useState} from "react";

import {Table, Modal, Button, Spinner} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";

import * as classes from './SubmissionEntryTable.module.scss';
import TimeInputTableRow from "../TimeInputTable/TimeInputTableRow/TimeInputTableRow";
import {useDispatch, useSelector} from "react-redux";
import {getAllLocations} from "../../../store/slices/time-input-submission-history-slice";
import {updateTimeInputRow} from "../../../store/slices/time-input-slice";
import TimeEntryApi from "../../../api/timeEntryApi";


const SubmissionEntryTable = (props)=> {

    const dispatch = useDispatch();

    const companyId = useSelector(state => state.auth.user.company);
    const locations = useSelector(state => state.timeInput.locations);

    const [show, setShow] = useState(false)
    const [selectedRecord, setSelectedRecord] = useState();
    const [updating, setUpdating] = useState(false)

    const editRecord = (entry) => {
        setShow(true);
        setSelectedRecord(entry)
        console.log(entry)
    }

    const handleCancel = () => {
        setShow(false);
    }

    const handleUpdateRecord = () => {
        console.log('Update!');
        console.log(selectedRecord);
        setUpdating(true);
        TimeEntryApi.updateTimeEntryRecord(selectedRecord).then(() => {
            setShow(false);
            setUpdating(false);
            props.onEntryUpdated();
        });
    }

    const valueChangedHandler = (valueKey, existingData, value) => {
        console.log(existingData)
        console.log(value)
        switch (valueKey) {
            case 'hours':
                console.log(value)
                setSelectedRecord({...selectedRecord, hours: value})
                break;
            case 'location':
                console.log(value)
                setSelectedRecord({...selectedRecord, location: value})
                break
            case 'task':
                console.log(value)
                setSelectedRecord({...selectedRecord, task: value})
                break;
            case 'notes':
                console.log(value)
                setSelectedRecord({...selectedRecord, notes: value})
                break;
        }
    }

    return (
        <>
        <Table responsive={true} striped={true} size={'sm'}>
            <thead>
            <tr>
                <th>Name</th>
                <th>Hours</th>
                <th>Location</th>
                <th>Task</th>
                <th>Notes</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            {props.entries.map((entry) => {
                return (
                    <tr>
                        <td>{entry.user.name.firstname + ' ' + entry.user.name.lastname}</td>
                        <td>{entry.hours}</td>
                        <td>{entry.location.name}</td>
                        <td>{entry.task.name}</td>
                        <td>{entry.notes}</td>
                        <td><FontAwesomeIcon className={classes.EditIcon} icon={faEdit} onClick={() => editRecord(entry)}/></td>
                    </tr>
                )
            })}
            </tbody>
        </Table>
            <Modal
                show={show}
                onHide={handleCancel}
                backdrop="static"
                keyboard={false}
                aria-labelledby="contained-modal-title-vcenter"
                centered
                dialogClassName={classes.Modal}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Update Record</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table responsive={true}>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Hours</th>
                            <th>Location</th>
                            <th>Task</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <TimeInputTableRow
                                           rowData={
                                               {
                                                   user: selectedRecord ? selectedRecord.user.id : '',
                                                   name: selectedRecord ? selectedRecord.user.name : '',
                                                   hours: selectedRecord ? selectedRecord.hours : 0,
                                                   location: selectedRecord ? selectedRecord.location.id : '',
                                                   task: selectedRecord ? selectedRecord.task.id : '',
                                                   notes: selectedRecord ? selectedRecord.notes : '',
                                                   date: selectedRecord ? selectedRecord.date : Date.now()
                                               }
                                           }
                                           locations={locations}
                                           hoursChangedHandler={(existingData, value) => valueChangedHandler('hours', existingData, value)}
                                           locationChangedHandler={(existingData, value) => valueChangedHandler('location', existingData, value)}
                                           taskChangedHandler={(existingData, value) => valueChangedHandler('task', existingData, value)}
                                           notesChangedHandler={(existingData, value) => valueChangedHandler('notes', existingData, value)}>
                        </TimeInputTableRow>
                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCancel} disabled={updating}>
                        Cancel
                    </Button>
                    {updating ? <Spinner animation={'border'} variant={'primary'}/> : <Button variant="primary" onClick={handleUpdateRecord}>Update Record</Button>}
                </Modal.Footer>
            </Modal>
            </>
    )
}

export default SubmissionEntryTable;