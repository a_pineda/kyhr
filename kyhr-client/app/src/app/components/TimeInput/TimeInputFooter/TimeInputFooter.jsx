import React from "react";

import * as classes from './TimeInputFooter.module.scss';
import CustomDatePicker from "../../UI/DatePicker/CustomDatePicker";
import Button from "../../UI/Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {submitDailyReport} from "../../../store/slices/time-input-slice";

const TimeInputFooter = (props) => {

    const timeEntries = useSelector(state => state.timeInput.timeInputRows);
    const selectedDate = useSelector(state => state.timeInput.selectedDate);
    const entryIsValid = useSelector(state => state.timeInput.entryIsValid);
    const dispatch = useDispatch();

    const submitHandler = () => {
        dispatch(submitDailyReport({date: selectedDate, entries: timeEntries}));
    }

    return (
        <div className={classes.TimeInputFooter}>
            <CustomDatePicker/>
            <Button btnType={Button.Types.success} clicked={() => submitHandler()} disabled={!entryIsValid} >Submit</Button>
        </div>
    )
}

export default TimeInputFooter;