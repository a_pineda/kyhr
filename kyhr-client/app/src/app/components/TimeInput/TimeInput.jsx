import React, {useEffect} from "react";

import * as classes from "./TimeInput.module.scss";
import TimeInputFooter from "./TimeInputFooter/TimeInputFooter";
import TimeInputTable from "./TimeInputTable/TimeInputTable";
import {useDispatch, useSelector} from "react-redux";
import {getAllLocations, getAllUsers, setSubmittedFalse} from "../../store/slices/time-input-slice";
import LoadingIcon from "../UI/LoadingIcon/LoadingIcon";
import {Jumbotron, Button, Tabs, Tab, TabContent, Nav} from "react-bootstrap";
import TimeInputSubmissionHistory from "./TimeInputSubmissionHistory/TimeInputSubmissionHistory";

const TimeInput = (props) => {

    const dispatch = useDispatch()
    const companyId = useSelector(state => state.auth.user.company);
    const users = useSelector(state => state.timeInput.users);
    const locations = useSelector(state => state.timeInput.locations);
    const submissionStatus = useSelector(state => state.timeInput.submissionStatus);

    useEffect(() => {
        dispatch(getAllUsers(companyId));
        dispatch(getAllLocations(companyId));
    }, [dispatch, companyId]);

    const resetTimeInput = () => {
        dispatch(setSubmittedFalse())
    }

    return (
        <Tab.Container defaultActiveKey={1}>
            <Nav variant={"tabs"}>
                <Nav.Item>
                    <Nav.Link eventKey={1}>Time Input</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey={2}>Submission History</Nav.Link>
                </Nav.Item>
            </Nav>
            <Tab.Content className={classes.TabContent}>
                <Tab.Pane className={classes.TabPane} eventKey={1}>
                    <div className={classes.TimeInput}>
                        {submissionStatus === 'not-submitted' &&
                        <>
                            <TimeInputTable users={users} locations={locations}/>
                            <TimeInputFooter submitDisabled={false}/>
                        </>
                        }
                        {submissionStatus === 'pending' &&
                        <>
                            <LoadingIcon text={'Submitting'}/>
                        </>
                        }
                        {submissionStatus === 'submitted' &&
                        <>
                            <Jumbotron>
                                <h1>Success!</h1>
                                <p>
                                    Your submission was successful! Click "Submit Another" if you would like to create a new submission.
                                </p>
                                <p>
                                    <Button variant="primary" onClick={resetTimeInput}>Submit Another</Button>
                                </p>
                            </Jumbotron>
                        </>
                        }
                        {submissionStatus === 'rejected' &&
                        <>
                            <Jumbotron>
                                <h1>Error!</h1>
                                <p>
                                    There was an error processing your request. Please check your inputs and try again.
                                </p>
                                <p>
                                    <Button variant="primary" onClick={resetTimeInput}>Retry</Button>
                                </p>
                            </Jumbotron>
                        </> }
                    </div>
                </Tab.Pane>
                <Tab.Pane className={classes.TabPane} eventKey={2}>
                    <TimeInputSubmissionHistory/>
                </Tab.Pane>
            </Tab.Content>
        </Tab.Container>
    )

}

export default TimeInput;