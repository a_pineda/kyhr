import React from "react";

import * as classes from './LoadingIcon.module.scss';

const LoadingIcon = (props) => {

    return (
        <div className={classes.LoadingIcon}>
            <div className={classes.LoadingIconInner}>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div className={classes.LoadingIconLabel}>
                {props.text}
            </div>
        </div>
    )

}

export default LoadingIcon;