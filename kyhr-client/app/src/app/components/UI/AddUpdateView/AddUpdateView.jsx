import React from "react";

import * as classes from './AddUpdateView.module.scss';
import Button from "../Button/Button";
import {Spinner} from "react-bootstrap";

const AddUpdateView = (props) => {

    let mainButtonText = 'Add New';

    if(props.updating) {
        mainButtonText = `Update`;
    }

    return (
        <div className={classes.AddUpdateView}>
            {props.children}
            <div className={classes.AddUpdateButton}>
                {props.submitStatus === 'pending' ? <Spinner animation={'border'}/>
                    :
                <Button disabled={props.addUpdateButtonDisabled} clicked={props.onAddUpdateClicked} btnType={Button.Types.success}>{mainButtonText}</Button>}
            </div>
        </div>
    )
}

export default AddUpdateView;