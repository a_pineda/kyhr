import React from "react";

import * as classes from './MasterListDetail.module.scss';
import MasterListDetailList from "./MasterListDetailList/MasterListDetailList";
import MasterListDetailView from "./MasterListDetailView/MasterListDetailView";
import {Container, Row, Col} from "react-bootstrap";


const MasterListDetail = (props) => {

    return (
        <Container fluid className={[classes.MasterListDetail, 'p-0', 'm-0', 'h-100']}>
            <Row noGutters className={['flex-grow-1', 'h-100']}>
                <Col xl={3} lg={4} md={4} className={['h-100']}>
                    <MasterListDetailList
                        listTitle={props.listTitle}
                        listItems={props.listItems}
                        listItemClicked={props.listItemClicked}
                        addButton={props.addButton}
                        addButtonClicked={props.addButtonClicked}
                        displayName={props.displayName}/>
                </Col>
                <Col xl={9} lg={8} md={8} className={['h-100']}>
                    <MasterListDetailView>
                        {props.children}
                    </MasterListDetailView>
                </Col>
            </Row>
        </Container>
    )
}

export default MasterListDetail;