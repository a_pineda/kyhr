import React from "react";

import * as classes from './MasterListDetailList.module.scss';
import ListHeader from "./ListHeader/ListHeader";
import ListBody from "./ListBody/ListBody";
import Button from "../../Button/Button";


const MasterListDetailList = (props) => {

    return (
            <div className={classes.MasterListDetailList}>
                <div className={classes.List}>
                    <ListHeader headerText={props.listTitle}/>
                    <ListBody listItems={props.listItems} listItemClicked={props.listItemClicked} displayName={props.displayName} />
                    {props.addButton ? <Button clicked={() => props.addButtonClicked()} btnType={Button.Types.success}>Add New</Button> : <></>}
                </div>
                <div className={classes.Seperator}/>
            </div>

    )
}

export default MasterListDetailList;