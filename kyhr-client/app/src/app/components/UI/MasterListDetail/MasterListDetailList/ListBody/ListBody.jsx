import React from "react";

import {Accordion, Card} from "react-bootstrap";

import * as classes from './ListBody.module.scss';

const ListBody = (props) => {

    const activeListItems = props.listItems.filter((listItem) => {
        if(!listItem.active) {
            return false;
        }
        return true
    }).map((listItem) => {
        return (
            <li className={classes.ListItem} key={listItem.id} onClick={() => props.listItemClicked(listItem)}>{props.displayName(listItem)}</li>
        )
    })

    const inActiveListItems = props.listItems.filter((listItem) => {
        if(listItem.active) {
            return false;
        }
        return true
    }).map((listItem) => {
        return (
            <li className={classes.ListItem} key={listItem._id} onClick={() => props.listItemClicked(listItem)}>{props.displayName(listItem)}</li>
        )
    })

    return (
        <div className={classes.ListBody}>
            <Accordion defaultActiveKey={"0"}>
                <Card>
                    <Card.Header className={classes.CardHeader}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Active
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body>
                            <ul>
                                {activeListItems}
                            </ul>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
                <Card>
                    <Card.Header className={classes.CardHeader}>
                        <Accordion.Toggle as={Card.Header} eventKey="1">
                            Inactive
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="1">
                        <Card.Body>
                            <ul>
                                {inActiveListItems}
                            </ul>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    )
}


export default ListBody;