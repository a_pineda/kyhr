import React from "react";

import * as classes from './ListHeader.module.scss';


const ListHeader = (props) => {
    return (
        <div className={classes.ListHeader}>
            {props.headerText}
            <div className={classes.ListHeaderLine}></div>
        </div>
    )
}


export default ListHeader;