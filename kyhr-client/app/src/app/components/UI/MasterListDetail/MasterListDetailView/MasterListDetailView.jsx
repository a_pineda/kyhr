import React from "react";

import * as classes from './MasterListDetailView.module.scss';

const MasterListDetailView = (props) => {
    return (
        <div className={classes.MasterListDetailView}>
            {props.children}
        </div>
    )
}


export default MasterListDetailView;