import React from 'react';
import PropTypes from 'prop-types';

import classes from './Button.module.scss';

/**
 * Custom Button Component
 */
const Button = (props) => {
    return (
        <button
            disabled={props.disabled}
            className={[classes.Button, classes[props.btnType], classes[props.hidden ? 'Hidden' : '']].join(' ')}
            onClick={props.clicked}>{props.children}</button>
    );
}

Button.Types = {
    success: 'Success',
    danger: 'Danger',
    default: 'Default'
}

Button.propTypes = {
    /** Disable the button */
    disabled: PropTypes.bool,
    /** Values: Default, Success, Danger, Play, Pause, Stop */
    btnType: PropTypes.string,
    /** Hide the button */
    hidden: PropTypes.bool,
    /** Function to execute onClick */
    clicked: PropTypes.func,
    /** Button text or content */
    children: PropTypes.string.isRequired
}

Button.defaultProps = {
    disabled: false,
    btnType: 'Default',
    hidden: false
}

export default Button;