import React from 'react';

/**
 * Application Logo
 */
const Logo = (props) => (
    <img
        src={props.src}
        width={props.width}
        height={props.height}
        className="d-inline-block align-top"
        alt="IEC Logo"
    />
);

export default Logo;