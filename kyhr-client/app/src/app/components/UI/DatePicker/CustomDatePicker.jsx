import React from "react";
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker-min.module.css';

import * as classes from './CustomDatePicker.module.scss';
import { useDispatch, useSelector } from "react-redux";
import { updateSelectedDate } from "../../../store/slices/time-input-slice";

const CustomDatePicker = (props) => {

    const dispatch = useDispatch();
    const selectedDate = useSelector(state => state.timeInput.selectedDate);

    return (
        <div className={classes.CustomDatePicker}>
            <span>Select the Date: </span>
            <DatePicker selected={selectedDate} onChange={date => dispatch(updateSelectedDate(new Date(date).getTime()))} />
        </div>
    )
}

export default CustomDatePicker;