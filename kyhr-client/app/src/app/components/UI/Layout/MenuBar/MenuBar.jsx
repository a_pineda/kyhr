import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";

import * as classes from "./MenuBar.module.scss";
import {useDispatch} from "react-redux";
import Logo from "../../Logo/Logo";
import logo from '../../Logo/iec-logo-black-nobg.png';
import * as actions from '../../../../store/slices/auth-slice';
import {Navbar, Nav} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import NavigationItems from "../../../Navigation/NavigationItems/NavigationItems";

const MenuBar = (props) => {

    const dispatch = useDispatch();

    return (

        <Navbar collapseOnSelect expand={"lg"} className={classes.MenuBar}>
            <Navbar.Brand as={NavLink} to={"/"}>
                <Logo
                    src={logo}
                    width="150"
                    height="40"
                    className="d-inline-block align-top"
                    alt="IEC Logo"
                />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav" className={'justify-content-end'}>
                <div className={classes.MobileNav}><NavigationItems/></div>
                <Nav>
                    <Navbar.Text>
                        <div className={classes.Logout} onClick={() => dispatch(actions.logout())}>Logout <FontAwesomeIcon icon={faSignOutAlt}/></div>
                    </Navbar.Text>
                </Nav>
            </Navbar.Collapse>




        </Navbar>
    )

}

export default MenuBar;