import React from "react";
import { Switch, Route } from 'react-router-dom';

import * as classes from './Layout.module.scss';
import MenuBar from "./MenuBar/MenuBar";
import SideMenu from "./SideMenu/SideMenu";
import MainContent from "./MainContent/MainContent";
import Dashboard from "../../Dashboard/Dashboard";
import TimeInput from "../../TimeInput/TimeInput";
import CompanyManagement from "../../CompanyManagement/CompanyManagement";
import LocationManagement from "../../LocationManagement/LocationManagement";
import TaskManagement from "../../TaskManagement/TaskManagement";
import UserManagement from "../../UserManagement/UserManagement";
import ErrorPage from "../../ErrorPage/ErrorPage";
import UserProfile from "../../UserProfile/UserProfile";
import {Row, Col} from "react-bootstrap";
import AuthRoute from "../../hoc/AuthRoute/AuthRoute";
import Utils from "../../../utils/utils";

const Layout = (props) => {

    return (

        <Row noGutters className={['flex-grow-1', 'h-100']}>
            <Col className={classes.RootCol}>
            <Row noGutters>
                <Col>
                    <MenuBar/>
                </Col>
            </Row>
            <Row noGutters className={[classes.LayoutContainer, 'flex-grow-1']}>
                <Col xl={2} lg={2} className={classes.SideMenuCol}>
                    <SideMenu/>
                </Col>
                <Col xl={10} lg={10} className={classes.MainContentCol}>
                    <MainContent>
                        <Switch>
                            <AuthRoute roles={[Utils.userRoles.user.role, Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/dashboard" component={Dashboard}/>
                            <AuthRoute roles={[Utils.userRoles.user.role, Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/" component={Dashboard}/>
                            <AuthRoute roles={[Utils.userRoles.user.role, Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/myprofile" component={UserProfile}/>
                            <AuthRoute roles={[Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/manager/timeinput" component={TimeInput}/>
                            <AuthRoute roles={[Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/manager/locationmanagement" component={LocationManagement}/>
                            <AuthRoute roles={[Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/manager/taskmanagement" component={TaskManagement}/>
                            <AuthRoute roles={[Utils.userRoles.manager.role, Utils.userRoles.admin.role,]} exact path="/manager/usermanagement" component={UserManagement}/>
                            <AuthRoute roles={[Utils.userRoles.admin.role]} exact path="/admin/companymanagement" component={CompanyManagement}/>
                            <Route exact path="/error" component={ErrorPage}/>
                            <Route render={(props) => (
                                <ErrorPage statusCode={404} errorMessage={'Page Not Found'}/>
                            )}/>
                        </Switch>
                    </MainContent>
                </Col>
            </Row>
            </Col>
        </Row>
    )

}

export default Layout;