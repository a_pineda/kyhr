import React from "react";

import * as classes from "./SideMenu.module.scss";
import NavigationItems from "../../../Navigation/NavigationItems/NavigationItems";
import {useSelector} from "react-redux";

const SideMenu = (props) => {

    const user = useSelector(state => state.auth.user)

    return (
            <div className={classes.SideMenu}>
                <div className={classes.User}>
                    <span className={classes.UserIcon}>{user.name.firstname.charAt(0).toUpperCase()}</span>
                    <span className={classes.Username}>{user.name.firstname + ' ' + user.name.lastname}</span>
                </div>
                <NavigationItems/>
            </div>
    )

}

export default SideMenu;