import React from "react";

import * as classes from "./MainContent.module.scss";

const MainContent = (props) => {

    return (
        <div className={classes.MainContent}>{props.children}</div>
    )

}

export default MainContent;