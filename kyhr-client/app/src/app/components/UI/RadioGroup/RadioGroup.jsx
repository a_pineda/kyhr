import React from "react";

import * as classes from './RadioGroup.module.scss'
import {useSelector} from "react-redux";
import RadioGroupItem from "./RadioGroupItem/RadioGroupItem";


const RadioGroup = (props) => {

    const currentUserRole = useSelector(state => state.auth.user.role)

    const options = props.options;

    let radioOptionArray = [];
    for (let key in options) {
        radioOptionArray.push({
            id: key,
            ...options[key]
        });
    }

    const radioOptions = radioOptionArray.map(radioOption => (

        <RadioGroupItem
            restrictToRoles={radioOption.restrictToRoles}
            currentUserRole={currentUserRole}
            key={radioOption.id}
            type={'radio'}
            id={radioOption.id}
            name={radioOption.name}
            value={radioOption.value}
            label={radioOption.label}
            onChange={props.onChange}
            checked={radioOption.checked}
            disabled={radioOption.disabled}
        />

    ));

    return (
        <div className={classes.RadioGroup}>
            {radioOptions}
        </div>
    )
}

export default RadioGroup;