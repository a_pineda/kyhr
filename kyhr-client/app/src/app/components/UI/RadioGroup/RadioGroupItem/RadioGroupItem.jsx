import React from "react";

import * as classes from './RadioGroupItem.module.scss'

const RadioGroupItem = (props) => {

    return (
        <div className={classes.RadioGroupItem}>
            <input
                hidden={!props.restrictToRoles.includes(props.currentUserRole)}
                key={props.id}
                type={'radio'}
                id={props.id}
                name={props.name}
                value={props.value}
                onChange={props.onChange}
                checked={props.checked}
                disabled={props.disabled}
            />
            <label hidden={!props.restrictToRoles.includes(props.currentUserRole)} htmlFor={props.label.labelFor}>{props.label.text}</label>
        </div>
    )
}

export default RadioGroupItem;