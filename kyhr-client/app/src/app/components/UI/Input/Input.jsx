import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from 'prop-types';

import classes from './Input.module.scss';

/**
 * Custom Input Control Component
 */
const Input = ( props ) => {
    let inputElement = null;
    const inputClasses = [classes.InputElement];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);
    }

    let icon = <></>;

    if(props.elementConfig.icon) {
        icon = (
            <span className={classes.FieldIcon}>
                <FontAwesomeIcon icon={props.elementConfig.icon}/>
            </span>
        )
    }

    switch ( props.elementType ) {
        case ( 'input' ):
            inputElement = (
                <div className={classes.Field}>
                    {icon}
                    <input
                        className={inputClasses.join(' ')}
                        {...props.elementConfig}
                        value={props.value}
                        onChange={props.changed}/>
                </div>);
            break;
        case ( 'textarea' ):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case ( 'select' ):
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    value={props.value}
                    onChange={props.changed}>
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value} disabled={option.disabled}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>
            );
            break;
        case ('checkbox'):
            inputElement = (
                <>
                    <input
                        type={'checkbox'}
                        {...props.elementConfig}
                        checked={props.elementConfig.checked}
                        onChange={props.changed}
                    />
                    <label>{props.elementConfig.label}</label>
                </>
            )
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }

    return (
        <div className={classes.Input}>
            <label className={classes.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
};

Input.propTypes = {
    /** Element type - Values: input, textarea, select */
    elementType: PropTypes.string.isRequired,
    /** Element type - Values: input, textarea, select */
    elementConfig: PropTypes.object.isRequired,
    /** Element type - Values: input, textarea, select */
    value: PropTypes.string.isRequired,
    /** Element type - Values: input, textarea, select */
    invalid: PropTypes.bool.isRequired,
    /** Element type - Values: input, textarea, select */
    shouldValidate: PropTypes.object.isRequired,
    /** Touched */
    touched: PropTypes.bool.isRequired,
    /** Function to call onChange */
    changed: PropTypes.func.isRequired
}

export default Input;