import React from "react";
import {useSelector} from "react-redux";

import * as classes from './UserProfile.module.scss';

const UserProfile = (props) => {

    const currentUser = useSelector(state => state.auth.user);

    return (
        <div className={classes.UserProfile}>
            <h4 >Welcome, {currentUser.name.firstname + ' ' + currentUser.name.lastname}!</h4>
        </div>
    )
}

export default UserProfile;