import React, {useEffect, useRef, useState} from "react";

import {useDispatch, useSelector} from "react-redux";
import MasterListDetail from "../UI/MasterListDetail/MasterListDetail";
import {getAllUsers, setSelectedUser, setTempUser, setUpdating, setUpdatingPassword} from "../../store/slices/user-management-slice";
import AddUpdateView from "../UI/AddUpdateView/AddUpdateView";
import {Accordion, Col, Form, Card} from "react-bootstrap";
import * as userActions from "../../store/slices/user-management-slice";

const UserManagement = (props) => {

    const dispatch = useDispatch();

    const currentUser = useSelector(state => state.auth.user)
    const companyId = useSelector(state => state.auth.user.company);
    const users = useSelector(state => state.userManagement.users);
    const selectedUser = useSelector(state => state.userManagement.selectedUser);
    const tempUser = useSelector(state => state.userManagement.tempUser);
    const updating = useSelector(state => state.userManagement.updating);
    const updatingPassword = useSelector(state => state.userManagement.updatingPassword);
    const submitStatus = useSelector(state => state.userManagement.submitStatus);

    const userInfoFormElement = useRef(null);
    const employeeInfoFormElement = useRef(null);
    const passwordFormElement = useRef(null);

    const [userInfoFormValidated, setUserInfoFormValidated] = useState(false);
    const [employeeInfoFormValidated, setEmployeeInfoFormValidated] = useState(false);
    const [passwordInfoFormValidated, setPasswordInfoFormValidated] = useState(false);
    const [accordionActiveKey, setAccordionActiveKey] = useState('0')
    const [passwordInfo, setPasswordInfo] = useState({
        currentPassword: '',
        newPassword: '',
        newPasswordConfirm: ''
    })

    useEffect(() => {
        dispatch(getAllUsers(companyId));
    }, [dispatch, companyId])

    useEffect(() => {
        dispatch(setTempUser({
            company: companyId,
            name: {
                firstname: '',
                lastname: ''
            },
            phone: '',
            email: '',
            role: 'user',
            active: true
        }));
    }, [dispatch, companyId])

    useEffect(() => {
        if(updating && !updatingPassword) {
           setAccordionActiveKey('1');
        }
    }, [accordionActiveKey, updating, updatingPassword])

    const onListItemClicked = (user) => {
        dispatch(setSelectedUser(user))
        dispatch(setTempUser(user))
        dispatch(setUpdating(true))
        dispatch(setUpdatingPassword(false));
        setUserInfoFormValidated(false);
        setEmployeeInfoFormValidated(false);
        setPasswordInfoFormValidated(false);
        setAccordionActiveKey('1');
    }

    const onAddButtonClicked = () => {
        dispatch(setSelectedUser(undefined));
        dispatch(setTempUser({
            company: companyId,
            name: {
                firstname: '',
                lastname: ''
            },
            phone: '',
            email: '',
            role: 'user',
            active: true
        }));
        dispatch(setUpdating(false));
        dispatch(setUpdatingPassword(false));
        setUserInfoFormValidated(false);
        setEmployeeInfoFormValidated(false);
        setPasswordInfoFormValidated(false);
        setAccordionActiveKey('0');

    }

    const inputChangedHandler = (event) => {
        const name = tempUser.name;
        switch (event.target.id) {
            case 'firstname':
                dispatch(setTempUser({...tempUser, name: {...name, firstname: event.target.value}}))
                break;
            case 'lastname':
                dispatch(setTempUser({...tempUser, name: {...name, lastname: event.target.value}}))
                break;
            case 'email':
                dispatch(setTempUser({...tempUser, email: event.target.value}))
                break;
            case 'phone':
                dispatch(setTempUser({...tempUser, phone: event.target.value}))
                break;
            case 'active-switch':
                dispatch(setTempUser({...tempUser, active: event.target.checked}))
                break;
            case 'user-role-select':
                dispatch(setTempUser({...tempUser, role: event.target.value}))
                break;
            case 'current-password':
                setPasswordInfo({...passwordInfo, currentPassword: event.target.value})
                break;
            case 'new-password':
                setPasswordInfo({...passwordInfo, newPassword: event.target.value})
                break;
            case 'new-password-confirm':
                setPasswordInfo({...passwordInfo, newPasswordConfirm: event.target.value})
                break;
            default:
                break;
        }
    }

    const addUpdateButtonDisabled = () => {
        return false;
    }

    const onAddUpdateClicked = () => {
        if(updating) {
            if(updatingPassword) {
                const passwordUpdate = {
                    userId: tempUser.id,
                    passwordCurrent: passwordInfo.currentPassword,
                    password: passwordInfo.newPassword,
                    passwordConfirm: passwordInfo.newPasswordConfirm
                }
                dispatch(userActions.updatePassword(passwordUpdate))
                setPasswordInfo({
                    currentPassword: '',
                    newPassword: '',
                    newPasswordConfirm: ''
                })
            } else {
                if(userInfoFormElement.current.checkValidity() && employeeInfoFormElement.current.checkValidity()) {
                    dispatch(userActions.updateUser(tempUser))
                }
                setUserInfoFormValidated(true);
                setEmployeeInfoFormValidated(true);
            }
        } else {
            console.log('Adding User')
            if(userInfoFormElement.current.checkValidity() && employeeInfoFormElement.current.checkValidity() && passwordFormElement.current.checkValidity()) {
                console.log('Passed')
                dispatch(userActions.addUser(
                    {
                        ...tempUser,
                        password: passwordInfo.newPassword,
                        passwordConfirm: passwordInfo.newPasswordConfirm
                    })
                )
                dispatch(setTempUser({
                    company: companyId,
                    name: {
                        firstname: '',
                        lastname: ''
                    },
                    phone: '',
                    email: '',
                    role: 'user',
                    active: true
                }));
                setPasswordInfo({
                    currentPassword: '',
                    newPassword: '',
                    newPasswordConfirm: ''
                })
            } else {
                setUserInfoFormValidated(true);
                setEmployeeInfoFormValidated(true);
            }
        }
    }

    const newPasswordIsValid = () => {
        return passwordInfo.newPassword.length >= 8
    }

    const newPasswordIsInvalid = () => {
        return passwordInfo.newPassword.length > 0 && passwordInfo.newPassword.length < 8
    }

    const newPasswordConfirmIsValid = () => {
        return newPasswordIsValid() && passwordInfo.newPassword === passwordInfo.newPasswordConfirm
    }

    const newPasswordConfirmIsInvalid = () => {
        return newPasswordIsValid() && passwordInfo.newPassword !== passwordInfo.newPasswordConfirm
    }

    const userInfoForm = (
        <Form ref={userInfoFormElement} noValidate validated={userInfoFormValidated}>
            <Form.Group>
                <Form.Row>
                    <Col>
                        <Form.Label>
                            First Name
                        </Form.Label>
                        <Form.Control required id={'firstname'} type={'text'} placeholder={'Firstname'} value={tempUser ? tempUser.name.firstname : ''} onChange={inputChangedHandler} disabled={updatingPassword}/>
                    </Col>
                    <Col>
                        <Form.Label>
                            Last Name
                        </Form.Label>
                        <Form.Control required id={'lastname'} type={'text'} placeholder={'Lastname'} value={tempUser ? tempUser.name.lastname : ''} onChange={inputChangedHandler} disabled={updatingPassword}/>
                    </Col>
                </Form.Row>
            </Form.Group>
            <Form.Group>
                <Form.Row>
                    <Col sm={6} md={6} lg={6} xl={6}>
                        <Form.Group>
                            <Form.Label>
                                Email Address
                            </Form.Label>
                            <Form.Control id={'email'} required type={'email'} placeholder={'Email Address'}
                                          value={tempUser ? tempUser.email : ''} onChange={inputChangedHandler} disabled={updatingPassword}/>
                        </Form.Group>
                    </Col>
                    <Col sm={6} md={6} lg={6} xl={6}>
                        <Form.Group>
                            <Form.Label>
                                Phone Number
                            </Form.Label>
                            <Form.Control id={'phone'} type={'tel'} placeholder={'345-123-4567'} pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                          value={tempUser ? tempUser.phone : ''}  onChange={inputChangedHandler} disabled={updatingPassword}/>
                        </Form.Group>
                    </Col>
                </Form.Row>
            </Form.Group>
        </Form>
    )

    const employeeInfoForm = (
        <Form ref={employeeInfoFormElement} noValidate validated={employeeInfoFormValidated}>
            <Form.Row>
                <Col sm={3} md={3} lg={3} xl={3}>
                    <Form.Label>
                        Active Status
                    </Form.Label>
                    <Form.Group>
                        <Form.Check id={'active-switch'} type={"switch"} label={"Active"} checked={tempUser ? tempUser.active : true}  onChange={inputChangedHandler} disabled={updatingPassword}/>
                    </Form.Group>
                </Col>
                <Col sm={6} md={6} lg={6} xl={6}>
                    <Form.Group>
                        <Form.Label>
                            User Role
                        </Form.Label>
                        <Form.Control id={'user-role-select'} as={'select'} onChange={inputChangedHandler} disabled={updatingPassword ||  currentUser.role !== 'admin'}>
                            <option selected={tempUser ? tempUser.role === 'user' : false} value={'user'}>User</option>
                            <option selected={tempUser ? tempUser.role === 'manager' : false} value={'manager'} hidden={currentUser.role !== 'admin'}>Manager</option>
                            <option selected={tempUser ? tempUser.role === 'admin' : false} value={'admin'} hidden={currentUser.role !== 'admin'}>Admin</option>
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Form.Row>
        </Form>
    )

    const passwordForm = (

                <Form ref={passwordFormElement} noValidate validated={passwordInfoFormValidated}>
                    <Form.Group hidden={!updating}>
                        <Form.Row>
                            <Col sm={6} md={6} lg={6} xl={6}>
                                <Form.Group>
                                    <Form.Label>
                                        Current Password
                                    </Form.Label>
                                    <Form.Control id={'current-password'} required={updatingPassword} type={'password'} placeholder={'Current Password'}
                                                  value={passwordInfo.currentPassword} disabled={accordionActiveKey === '1'} onChange={inputChangedHandler}/>
                                </Form.Group>
                            </Col>
                        </Form.Row>
                    </Form.Group>
                    <Form.Group>
                        <Form.Row>
                            <Col sm={6} md={6} lg={6} xl={6}>
                                <Form.Group>
                                    <Form.Label>
                                        New Password
                                    </Form.Label>
                                    <Form.Control id={'new-password'} required type={'password'} placeholder={'New Password'}
                                                  value={passwordInfo.newPassword} disabled={accordionActiveKey === '1'} onChange={inputChangedHandler} isValid={newPasswordIsValid()} isInvalid={newPasswordIsInvalid()}/>
                                </Form.Group>
                            </Col>
                            <Col sm={6} md={6} lg={6} xl={6}>
                                <Form.Group>
                                    <Form.Label>
                                        Confirm New Password
                                    </Form.Label>
                                    <Form.Control id={'new-password-confirm'} required type={'password'} placeholder={'Confirm New Password'}
                                                  value={passwordInfo.newPasswordConfirm} disabled={accordionActiveKey === '1'} onChange={inputChangedHandler} isValid={newPasswordConfirmIsValid()} isInvalid={newPasswordConfirmIsInvalid()}/>
                                </Form.Group>
                            </Col>
                        </Form.Row>
                    </Form.Group>
                </Form>


    )

    const startUpdatingPassword = (event) => {
        if(updating) {
            if(!updatingPassword) {
                dispatch(setUpdatingPassword(true))
            } else {
                dispatch(setUpdatingPassword(false));
            }
        }
        if(accordionActiveKey === '1') {
            setAccordionActiveKey('0')
        } else if(updating) {
            setAccordionActiveKey('1')
        }
    }

    const displayNameFunction = (user) => {
        return user.name.firstname + ' ' + user.name.lastname;
    }

    return (
            <MasterListDetail listTitle={'Users'} listItems={users} listItemClicked={onListItemClicked} addButton={true} addButtonClicked={onAddButtonClicked} displayName={displayNameFunction}>
                <AddUpdateView addUpdateButtonDisabled={addUpdateButtonDisabled()} onAddUpdateClicked={onAddUpdateClicked} updating={updating} submitStatus={submitStatus}>
                    <h3>User Information</h3>
                    {userInfoForm}
                    <h3>Employee Information</h3>
                    {employeeInfoForm}
                    <h3>{selectedUser ? 'Reset' : 'Set'} Password</h3>
                    <Accordion activeKey={accordionActiveKey}>
                        <Card>
                            <Accordion.Toggle onClick={startUpdatingPassword} as={Card.Header} eventKey={'0'}>
                                {updating ? 'Reset' : 'Set'} Password
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey='0'>
                                <Card.Body>
                                    {passwordForm}
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </AddUpdateView>
            </MasterListDetail>
    )
}

export default UserManagement;