import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    getAllLocations,
    updateLocation,
    addLocation,
    setTempLocation, setSelectedLocation, setUpdating
} from "../../store/slices/location-management-slice";
import AddUpdateView from "../UI/AddUpdateView/AddUpdateView";
import MasterListDetail from "../UI/MasterListDetail/MasterListDetail";
import {Form, Col, Row} from "react-bootstrap";
import Select from "react-select";
import {getAllTasks} from "../../store/slices/task-management-slice";

const LocationManagement = (props) => {

    const dispatch = useDispatch();

    const companyId = useSelector(state => state.auth.user.company);
    const locations = useSelector(state => state.locationManagement.locations);
    const tasks = useSelector( state => state.taskManagement.tasks);
    const selectedLocation = useSelector(state => state.locationManagement.selectedLocation);
    const tempLocation = useSelector(state => state.locationManagement.tempLocation);
    const submitStatus = useSelector(state => state.locationManagement.submitStatus);
    const updating = useSelector(state => state.locationManagement.updating);

    useEffect(() => {
        dispatch(getAllLocations(companyId));
        dispatch(getAllTasks(companyId));
    }, [dispatch, companyId])

    useEffect( () => {
        if(!updating) {
            dispatch(setTempLocation({
                company: companyId,
                name: '',
                active: true,
                tasks: [],
                populatedTasks: []
            }))
        }
    }, [])

    const onAddUpdateClicked = () => {
        if(selectedLocation) {
            dispatch(updateLocation(tempLocation))
        } else {
            dispatch(addLocation(tempLocation))
            dispatch(setUpdating(true));
        }
    }

    const inputChangedHandler = (event) => {
        switch (event.target.id) {
            case 'location-name':
                if(selectedLocation) {
                    dispatch(setTempLocation({
                        ...tempLocation,
                        name: event.target.value
                    }))
                } else {
                    dispatch(setTempLocation( {
                        ...tempLocation,
                        name: event.target.value
                    }))
                }
                break;
            case 'active-switch':
                dispatch(setTempLocation({
                    ...tempLocation,
                    active: event.target.checked
                }))
                break;
            default:
                break;
        }
    }

    const taskSelectChangedHandler = (event) => {
        const taskIdArray = event.map((option) => {
            return option.value;
        })
        dispatch(updateLocation({...tempLocation, tasks: taskIdArray}));
    }

    const tasksToOptions = tasks.map((task) => {
        return {label: task.name, value: task.id}
    })

    const tasksToValue = tempLocation ? tempLocation.populatedTasks.map((task) => {
        return {label: task.name, value: task.id}
    }) : []

    const onListItemClicked = (task) => {
        dispatch(setSelectedLocation({...task}));
        dispatch(setTempLocation({...task}));
        dispatch(setUpdating(true));
    }

    const onAddButtonClicked = () => {
        dispatch(setSelectedLocation(undefined));
        dispatch(setTempLocation( {
            company: companyId,
            name: '',
            active: true,
            tasks: [],
            populatedTasks: []
        }))
        dispatch(setUpdating(false));
    }

    const addUpdateButtonDisabled = () => {
        return submitStatus === 'pending';
    }

    const displayNameFunction = (location) => {
        return location.name;
    }

    const locationForm = (
        <Form>
            <Form.Group>
                <Form.Row>
                    <Col md={6} lg={6} xl={6}>
                        <Form.Label>Location Name</Form.Label>
                        <Form.Control id={'location-name'} type={'text'} value={tempLocation ? tempLocation.name : ''} name={'location'} placeholder={'Location Name'} onChange={inputChangedHandler}/>
                    </Col>
                </Form.Row>
            </Form.Group>
            <Form.Group>
                <Form.Row>
                    <Col md={6} lg={6} xl={6}>
                        <Form.Label>Active Status</Form.Label>
                        <Form.Check id={'active-switch'} type={"switch"} label={"Active"} checked={tempLocation ? tempLocation.active : true}  onChange={inputChangedHandler}/>
                    </Col>
                </Form.Row>
            </Form.Group>
        </Form>
    )

    return (
            <MasterListDetail listTitle={'Locations'} listItems={locations} listItemClicked={onListItemClicked} addButton={true} addButtonClicked={onAddButtonClicked} displayName={displayNameFunction}>
                <AddUpdateView addUpdateButtonDisabled={addUpdateButtonDisabled()} onAddUpdateClicked={onAddUpdateClicked} updating={updating}>
                    <h3>Location Information</h3>
                    {locationForm}
                    <h3>Linked Tasks</h3>
                    <Row>
                        <Col lg={6}>
                            <Form.Group>
                                <Select isSearchable isMulti options={tasksToOptions} value={tasksToValue} onChange={taskSelectChangedHandler}/>
                            </Form.Group>
                        </Col>
                    </Row>

                </AddUpdateView>
            </MasterListDetail>
    )
}

export default LocationManagement;