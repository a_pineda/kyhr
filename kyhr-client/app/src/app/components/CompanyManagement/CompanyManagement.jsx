import React, {useEffect, useState} from "react";

import * as classes from './CompanyManagement.module.scss';
import CompanyApi from "../../api/companyApi";
import {useSelector} from "react-redux";
import {
    InputGroup,
    FormControl,
    Col,
    Row,
    Container,
    Button,
    Spinner,
    ListGroup,
    Badge
} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons";


const CompanyManagement = (props) => {

    const [company, setCompany] = useState();
    const companyId = useSelector(state => state.auth.user.company);
    const [addingEmail, setAddingEmail] = useState(false);
    const [updatingName, setUpdatingName] = useState(false);
    const [removingEmail, setRemovingEmail] = useState(false);
    const [newEmail, setNewEmail] = useState('');

    const inputChangedHandler = (event) => {

        switch (event.target.id) {
            case 'company-name-input':
                setCompany({...company, name: event.target.value})
                break;
            case 'new-email-input':
                setNewEmail(event.target.value);
                break;
            default:
                break;
        }

    }

    useEffect(() => {
        CompanyApi.getOneCompany(companyId).then((res) => {
            if(res){
                setCompany(res.data.data.doc);
            }
        })
    }, [companyId])

    const onUpdateClickedHandler = () => {

        setUpdatingName(true);

        CompanyApi.updateCompany(company).then((res) => {
            if(res) {
                setCompany(res.data.data.data);
                setUpdatingName(false);
            }
        });
    }

    const onAddClickedHandler = () => {

        setAddingEmail(true);
        const notificationEmailAddresses = company ? [...company.notificationEmailAddresses] : [];

        notificationEmailAddresses.push(newEmail);
        setNewEmail('');

        const newCompany = {...company, notificationEmailAddresses: notificationEmailAddresses}

        CompanyApi.updateCompany(newCompany).then((res) => {
            if(res) {
                setCompany(res.data.data.data);
                setAddingEmail(false);
            }
        });
    }

    const removeEmailHandler = (email) => {
        setRemovingEmail(true);
        let notificationEmailAddresses = company ? [...company.notificationEmailAddresses] : [];

        const index = notificationEmailAddresses.findIndex((e) => e === email);
        notificationEmailAddresses.splice(index, 1)

        const updatedCompany = {...company, notificationEmailAddresses: notificationEmailAddresses}

        CompanyApi.updateCompany(updatedCompany).then((res) => {
            if(res) {
                setCompany(res.data.data.data);
                setRemovingEmail(false);
            }
        });
    }

    const emailIsValid = () => {
        const regex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
        const emailExists = company?.notificationEmailAddresses.find((e) => e === newEmail);
        return regex.test(newEmail) && !emailExists;
    }

    return (
        <Container className={classes.CompanyManagement}>
            <Row>
                <h3>Company Profile</h3>
            </Row>
            <Row>
                <Col md={6}>
                    <label htmlFor="comapny-name">Company Name</label>
                    <InputGroup>
                        <FormControl id={'company-name-input'} type={'text'} value={company?.name}
                                     onChange={inputChangedHandler}/>
                        <InputGroup.Append>
                            <Button variant={'primary'} onClick={onUpdateClickedHandler} disabled={updatingName}>{updatingName ?
                                <Spinner size={'sm'} animation={'border'}/> : 'Update'}</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col md={6}>
                    <label htmlFor={'notification-email-addresses'}>Notification Email Addresses</label>
                    <InputGroup>
                        <FormControl id={'new-email-input'} type={'email'} value={newEmail} isValid={emailIsValid()} isInvalid={!emailIsValid() && newEmail.length > 0}
                                     onChange={inputChangedHandler} placeholder={'Add a new email'}/>
                        <InputGroup.Append>
                            <Button variant={'primary'} onClick={onAddClickedHandler} disabled={addingEmail || !emailIsValid()}>{addingEmail ?
                                <Spinner size={'sm'} animation={'border'}/> : 'Add'}</Button>
                        </InputGroup.Append>
                    </InputGroup>
                    <ListGroup>
                        {company?.notificationEmailAddresses.map((email) => {
                            return (
                                <ListGroup.Item className={classes.EmailListItem} key={email}>
                                    <Row>
                                        <Col xs={5} sm={5} md={5}>
                                            <div className={classes.ListItemDataHeader}>Email</div>
                                            <Badge variant={'primary'}>{email}</Badge>
                                        </Col>
                                        <Col xs={7} sm={7} md={7} className={classes.EmailRemove}>
                                            {removingEmail ? <Spinner size={'sm'} animation={'border'}/> : <FontAwesomeIcon onClick={() => removeEmailHandler(email)} icon={faTimesCircle}/>}
                                        </Col>
                                    </Row>
                                </ListGroup.Item>
                            )
                        })}
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    )
}

export default CompanyManagement;