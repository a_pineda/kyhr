import axios from "axios";
import Utils from "../utils/utils";

const apiUrl = Utils.getApiServerUrl();

const TimeInputSubmissionRecordApi = {
    getAllTimeInputSubmissionRecordsForCompany: (company) => {
        return axios.get(`${apiUrl}/api/v1/timeInputSubmissionRecords/company/${company}`,{
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    createTimeInputSubmissionRecord: (timeInputSubmissionRecord) => {
        return axios.post(`${apiUrl}/api/v1/timeInputSubmissionRecords`,timeInputSubmissionRecord, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    }
}

export default TimeInputSubmissionRecordApi;