import axios from "axios";
import Utils from "../utils/utils";

const apiUrl = Utils.getApiServerUrl();

const LocationApi = {
    getAllLocations: () => {
        return axios.get(`${apiUrl}/api/v1/locations`,{
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    getAllLocationsForCompany: (company) => {
        return axios.get(`${apiUrl}/api/v1/locations/company/${company}`,{
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    createLocation: (location) => {
        return axios.post(`${apiUrl}/api/v1/locations`,location, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    updateOne: (updatedLocation) => {
        return axios.patch(`${apiUrl}/api/v1/locations/updateLocation`,updatedLocation, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    }

}

export default LocationApi;