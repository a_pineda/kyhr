import axios from "axios";
import Utils from "../utils/utils";

const apiUrl = Utils.getApiServerUrl();

const UserApi = {
    authenticate: (credentials) => {
        return axios.post(`${apiUrl}/api/v1/users/login`, credentials)
            .catch((error) => {
                return Promise.reject(error)
            })
    },
    logout: () => {
        return axios.get(`${apiUrl}/api/v1/users/logout`)
            .catch((error) => {
                return Promise.reject(error)
            })
    },
    getAllUsersForCompany: (company) => {
        return axios.get(`${apiUrl}/api/v1/users/company/${company}`,{
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    createNewUser: (newUser) => {
        return axios.post(`${apiUrl}/api/v1/users/`, newUser, {
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    updateUser: (updatedUser) => {
        return axios.patch(`${apiUrl}/api/v1/users/${updatedUser._id}`, updatedUser, {
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    updatePassword: (updatePasswordItems) => {
        return axios.patch(`${apiUrl}/api/v1/users/updateMyPassword/`, updatePasswordItems, {
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    }
}


export default UserApi