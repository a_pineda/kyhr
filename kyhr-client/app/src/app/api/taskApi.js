import axios from "axios";
import Utils from "../utils/utils";

const apiUrl = Utils.getApiServerUrl();

const TaskApi = {
    getAllTasks: () => {
        return axios.get(`${apiUrl}/api/v1/tasks`,{
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    getAllTasksForCompany: (company) => {
        return axios.get(`${apiUrl}/api/v1/tasks/company/${company}`,{
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    createTask: (task) => {
        return axios.post(`${apiUrl}/api/v1/tasks`,task, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    updateOne: (updatedTask) => {
        return axios.patch(`${apiUrl}/api/v1/tasks/updateTask`,updatedTask, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    }

}

export default TaskApi;