import axios from "axios";
import Utils from "../utils/utils";

const apiUrl = Utils.getApiServerUrl();

const CompanyApi = {
    getAllLocations: () => {
        return axios.get(`${apiUrl}/api/v1/company`,{
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    createCompany: (company) => {
        return axios.post(`${apiUrl}/api/v1/company`, company, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    getOneCompany: (companyId) => {
        return axios.get(`${apiUrl}/api/v1/company/${companyId}`,{
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    },
    updateCompany: (newCompany) => {
        return axios.patch(`${apiUrl}/api/v1/company/updateCompany`, newCompany, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    }

}

export default CompanyApi;