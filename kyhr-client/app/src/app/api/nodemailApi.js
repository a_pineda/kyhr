import axios from "axios";
import Utils from "../utils/utils";

const apiUrl = Utils.getApiServerUrl();

const NodemailApi = {
    sendDailyReportSubmissionNotification: (emailData) => {
        return axios.post(`${apiUrl}/api/v1/mail/dailyReportSubmission`, emailData, {
            headers: {
                authorization: 'Bearer ' + Utils.getJwtToken()
            }
        }).catch(err => console.log(err))
    }
}

export default NodemailApi;