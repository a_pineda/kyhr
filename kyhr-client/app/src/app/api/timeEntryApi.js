import axios from "axios";
import Utils from "../utils/utils";
import qs from "qs";

const apiUrl = Utils.getApiServerUrl();

const TimeEntryApi = {
    submitDailyReport: (timeEntries) => {
        return axios.post(`${apiUrl}/api/v1/timeentry/bulk`, timeEntries,
            {
            headers: {
                Authorization: 'Bearer ' + Utils.getJwtToken()
            }
            }).catch(err => console.log(err))
    },
    getTimeEntryRecords: (recordIds) => {
        return axios.post(`${apiUrl}/api/v1/timeentry/uuids`, recordIds,
            {
                headers: {
                    Authorization: 'Bearer ' + Utils.getJwtToken()
                }
            }).catch(err => console.log(err))
    },
    updateTimeEntryRecord: (record) => {
        return axios.patch(`${apiUrl}/api/v1/timeentry/`, record,
            {
                headers: {
                    Authorization: 'Bearer ' + Utils.getJwtToken()
                }
            }).catch(err => console.log(err))
    }

 }

 export default TimeEntryApi;