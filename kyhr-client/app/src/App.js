import React from 'react';
import './App.module.scss';
import Auth from "./app/components/Auth/Auth";
import Layout from "./app/components/UI/Layout/Layout";
import {useSelector} from "react-redux";

function App() {
  const authenticated = useSelector(state => state.auth.token);
  return !!authenticated ? <Layout/> : <Auth/>
}

export default App;
