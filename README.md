# KYHR

## Repo Structure:
**DO NOT push to master. Master is the live production branch!**
Develop is the main development branch.

## Technology Stack:
**Client:**

  - React
  - Redux
  
**API Server:**

  - Node.js
  - Express
  - Mongoose
  
**Database:**

  - MongoDB

## Build API Server & Client
1. Navigate to kyhr/kyhr-api/
2. Run *npm install*
3. When *npm install* completes, navigate to kyhr/kyhr-client/app/
4. Run *npm install*

**The project dependencies are now installed and you are ready to run the app**

## Run API Server & Client
1. Navigate to kyhr/kyhr-api
2. Run *npm start* -Leave this terminal window open. Closing it will terminate the server.
3. In a new terminal window navigate to kyhr/kyhr-client/app/
4. Run *npm start* -Leave this terminal window open. Closing it will terminate the React development server.
5. Open and browser and navigate to *localhost:3000* to access the app
5. You can log in with any account in the kyhr dev database, or create your own account via Postman. (You can use andres@corev.com with password as the password)

