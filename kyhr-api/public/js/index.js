/* eslint-disable */
import '@babel/polyfill';
import { login, logout } from './login';
import {} from './bootstrap/bootstrap.bundle.min';

const loginForm = document.querySelector('.form');
const logOutButton = document.querySelector('#logout-button');

if (loginForm) {
    loginForm.addEventListener('submit', e => {
        e.preventDefault();

        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;

        login(email, password);
    });
}

if (logOutButton) {
    logOutButton.addEventListener('click', logout);
}
