const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Task name is required']
        },
        company: {
            type: mongoose.Schema.ObjectId,
            ref: 'Company',
            required: [true, 'Task must belong to a Company']
        },
        active: {
            type: Boolean,
            default: true
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;