
const mongoose = require('mongoose');

const companySchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Name is required']
        },
        notificationEmailAddresses: {
            type: [String],
            required: false,
            unique: false,
            lowercase: true
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

const Company = mongoose.model('Company', companySchema);

module.exports = Company;
