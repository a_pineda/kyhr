const mongoose = require('mongoose');

const timeInputSubmissionRecordSchema = new mongoose.Schema(
    {
        submissionDate: {
            type: Date,
            required: true
        },
        forDate: {
            type: Date,
            required: true
        },
        submittedBy: {
            type: mongoose.Schema.ObjectId,
            ref: 'User',
            required: [true, 'SubmissionRecord must belong to a User']
        },
        company: {
            type: mongoose.Schema.ObjectId,
            ref: 'Company',
            required: [true, 'SubmissionRecord must belong to a Company']
        },
        entries: {
            type: [mongoose.Schema.ObjectId],
            ref: 'TimeEntry',
            required: [true, 'Submission Record must have at least one Time Entry']
        },
        status: {
            type: String,
            enum: ['submitted'],
            default: 'submitted'
        },
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

const TimeInputSubmissionRecord = mongoose.model('TimeInputSubmissionRecord', timeInputSubmissionRecordSchema);

module.exports = TimeInputSubmissionRecord;
