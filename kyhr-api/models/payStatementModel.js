const mongoose = require('mongoose');

const payStatementSchema = new mongoose.Schema(
    {
        periodStart: {
            type: Date,
            required: true
        },
        periodEnd: {
            type: Date,
            required: true
        },
        employeeName: {
            type: String,
            required: [true, 'Name is required']
        },
        baseRate: {
            type: Number,
            required: true,
            default: 0
        },
        overtimeRate: {
            type: Number,
            required: true,
            default: 0
        },
        currency: {
            type: String,
            lowercase: true,
            enum: ['usd', 'kyd'],
            default: 'usd'
        },
        regularHours: {
            type: Number,
            default: 0
        },
        overtimeHours: {
            type: Number,
            default: 0
        },
        holidayHours: {
            type: Number,
            default: 0
        },
        vacationHours: {
            type: Number,
            default: 0
        },
        sickHours: {
            type: Number,
            default: 0
        },
        pensionDeduction: {
            type: Number,
            required: true,
            default: 0
        },
        medicalDeduction: {
            type: Number,
            required: true,
            default: 0
        },
        otherAdjustment: {
            type: [Number]
        },
        otherAdjustmentMessage: {
            type: [String]
        },
        netPay: {
            type: Number,
            required: true
        },
        user: {
            type: mongoose.Schema.ObjectId,
            ref: 'User',
            required: [true, 'Pay statement must belong to a User']
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

const PayStatement = mongoose.model('PayStatement', payStatementSchema);

module.exports = PayStatement;
