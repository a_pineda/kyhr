const mongoose = require('mongoose');

const locationSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Location name is required']
        },
        company: {
            type: mongoose.Schema.ObjectId,
            ref: 'Company',
            required: [true, 'Location must belong to a Company']
        },
        tasks: {
            type: [mongoose.Schema.ObjectId],
            ref: 'Task'
        },
        active: {
            type: Boolean,
            default: true
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

locationSchema.virtual('populatedTasks', {
    ref: 'Task',
    foreignField: '_id',
    localField: 'tasks'
});

const Location = mongoose.model('Location', locationSchema);

module.exports = Location;
