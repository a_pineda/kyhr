const mongoose = require('mongoose');

const timeEntrySchema = new mongoose.Schema(
    {
        date: {
            type: Date,
            required: true
        },
        hours: {
            type: Number,
            required: true,
            default: 0
        },
        location: {
            type: mongoose.Schema.ObjectId,
            ref: 'Location',
            required: [true, 'Time Entry must belong to a Location']
        },
        task: {
            type: mongoose.Schema.ObjectId,
            ref: 'Task',
            required: [true, 'Time Entry must belong to a Task']
        },
        notes: {
            type: String
        },
        user: {
            type: mongoose.Schema.ObjectId,
            ref: 'User',
            required: [true, 'Pay statement must belong to a User']
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

const TimeEntry = mongoose.model('TimeEntry', timeEntrySchema);

module.exports = TimeEntry;
