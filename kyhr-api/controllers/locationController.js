const Location = require('../models/locationModel');
const catchAsync = require('./../utils/catchAsync');
const factory = require('./handlerFactory');

exports.getAllLocations = factory.getAll(Location);
exports.getLocation = factory.getOne(Location);
exports.createLocation = factory.createOne(Location);
exports.updateLocation = factory.updateOne(Location, 'populatedTasks');
exports.deleteLocation = factory.deleteOne(Location);

exports.getAllLocationsForCompany = catchAsync(async (req, res, next) => {

    const locations = await Location.find({'company': req.params.id}).populate('populatedTasks');

    //SEND RESPONSE
    res.status(200).json({
        status: 'success',
        results: locations.length,
        data: {
            locations
        }
    });
});