const TimeInputSubmissionRecord = require('../models/timeInputSubmissionRecordModel');
const catchAsync = require('./../utils/catchAsync');
const factory = require('./handlerFactory');

exports.getAllTimeInputSubmissionRecords = factory.getAll(TimeInputSubmissionRecord);
exports.getTimeInputSubmissionRecord = factory.getOne(TimeInputSubmissionRecord);
exports.createTimeInputSubmissionRecord = factory.createOne(TimeInputSubmissionRecord);
exports.updateTimeInputSubmissionRecord = factory.updateOne(TimeInputSubmissionRecord);
exports.deleteTimeInputSubmissionRecord = factory.deleteOne(TimeInputSubmissionRecord);

exports.getAllTimeInputSubmissionRecordsForCompany = catchAsync(async (req, res, next) => {

    const timeInputSubmissionRecords = await TimeInputSubmissionRecord.find({'company': req.params.id}).populate('submittedBy').sort('-forDate');

    //SEND RESPONSE
    res.status(200).json({
        status: 'success',
        results: timeInputSubmissionRecords.length,
        data: {
            timeInputSubmissionRecords
        }
    });
});