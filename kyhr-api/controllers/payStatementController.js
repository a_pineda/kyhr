const PayStatement = require('./../models/payStatementModel');
const factory = require('./handlerFactory');

exports.getAllPayStatements = factory.getAll(PayStatement);
exports.getPayStatement = factory.getOne(PayStatement);
exports.createPayStatement = factory.createOne(PayStatement);
exports.updatePayStatement = factory.updateOne(PayStatement);
exports.deletePayStatement = factory.deleteOne(PayStatement);
