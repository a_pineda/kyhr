const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');
const Nodemailer = require('./../utils/nodemailer');
const fs = require('fs');
const path = require('path');

exports.sendDailyReportSubmittedEmail = catchAsync(async (req, res, next) => {

    fs.promises.mkdir(path.join(__dirname, '..', 'tmp', `${req.id}`), {recursive: true}, err => {
        if (err) {
            return console.error(err);
        }
    }).then(() => {
        const csvData =  [...req.body.csvData];
        csvData.unshift(['Name', 'Hours', 'Location', 'Task', 'Notes']);
        csvData.unshift(['For Date', new Date(req.body.date).toLocaleDateString()]);
        const csv = csvData.map((line) => {
            return line.join()
        }).join('\n');



        fs.promises.writeFile(path.join(__dirname, '..', 'tmp', `${req.id}`, 'data.csv'), csv, err => {
            if (err) {
                return console.error(err)
            }
        }).then( () => {

            Nodemailer.sendDailyReportSubmittedEmail(path.join(__dirname, '..', 'tmp', `${req.id}`, 'data.csv'), Nodemailer.csvToHtmlTable(req.body.csvData, new Date(req.body.date).toLocaleDateString()), req.body.receivers, new Date(req.body.date).toLocaleDateString()).then(() => {

                fs.rmdir(path.join(__dirname, '..', 'tmp', `${req.id}`), {recursive: true}, err => {
                    if (err) {
                        return console.error(err);
                    }
                });

                //SEND RESPONSE
                res.status(200).json({
                    status: 'success'
                });

            });
        })
    })
});