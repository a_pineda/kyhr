const TimeInput = require('../models/timeEntryModel');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');

const factory = require('./handlerFactory');

exports.getAllTimeEntries = factory.getAll(TimeInput);
exports.getTimeEntry = factory.getOne(TimeInput);
exports.createTimeEntry = factory.createOne(TimeInput);
exports.updateTimeEntry = factory.updateOne(TimeInput);
exports.deleteTimeEntry = factory.deleteOne(TimeInput);

exports.getTimeEntriesByIds = catchAsync(async (req, res, next) => {

    console.log('here');
    console.log(req.body);

    let entries = await TimeInput.find().where('_id').in(req.body).populate('user task location').exec();

    res.status(200).json({
        status: 'success',
        results: '',
        data: entries
    });

});

exports.createBulkEntries = catchAsync(async (req, res, next) => {

    if (!req.body) {
        return next(
            new AppError(
                'No data was supplied in the request body.',
                400
            )
        );
    }

    let entries = await TimeInput.insertMany(req.body.filter((entry) => {
        if(!entry.location || !entry.task) {
            return false;
        }
        return true;
    }))
    let retEntries = await TimeInput.find({_id: {$in: entries}}).populate('location task user')

    //SEND RESPONSE
    res.status(200).json({
        status: 'success',
        results: '',
        data: retEntries
    });
});