const Company = require('../models/companyModel');

const factory = require('./handlerFactory');

exports.getAllCompanies = factory.getAll(Company);
exports.getCompany = factory.getOne(Company);
exports.createCompany = factory.createOne(Company);
exports.updateCompany = factory.updateOne(Company);
exports.deleteCompany = factory.deleteOne(Company);