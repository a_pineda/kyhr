const express = require('express');
const timeInputSubmissionRecordController = require('../controllers/timeInputSubmissionRecordController');
const authController = require('./../controllers/authController');

const router = express.Router();

router
    .route('/')
    .get(authController.protect, timeInputSubmissionRecordController.getAllTimeInputSubmissionRecords)
    .post(
        authController.protect,
        authController.restrictTo('manager', 'admin'),
        timeInputSubmissionRecordController.createTimeInputSubmissionRecord
    );

router
    .route('/company/:id')
    .get( authController.protect, timeInputSubmissionRecordController.getAllTimeInputSubmissionRecordsForCompany);

module.exports = router;