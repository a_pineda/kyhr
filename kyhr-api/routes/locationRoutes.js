const express = require('express');
const locationController = require('./../controllers/locationController');
const authController = require('./../controllers/authController');

const router = express.Router();

router
    .route('/')
    .get(authController.protect, locationController.getAllLocations)
    .post(
        authController.protect,
        authController.restrictTo('manager', 'admin'),
        locationController.createLocation
    );

router
    .route('/company/:id')
    .get( authController.protect, locationController.getAllLocationsForCompany);

router.patch('/updateLocation', authController.protect, authController.restrictTo('manager', 'admin'), locationController.updateLocation);


module.exports = router;