const express = require('express');
const payStatementController = require('./../controllers/payStatementController');
const authController = require('./../controllers/authController');

const router = express.Router();

router
    .route('/')
    .get(authController.protect, payStatementController.getAllPayStatements)
    .post(
        authController.protect,
        authController.restrictTo('admin'),
        payStatementController.createPayStatement
    );

module.exports = router;
