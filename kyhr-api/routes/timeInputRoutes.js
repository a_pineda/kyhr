const express = require('express');
const timeInputController = require('./../controllers/timeInputController');
const authController = require('./../controllers/authController');

const router = express.Router();

router
    .route('/bulk')
    .post(
        authController.protect,
        authController.restrictTo('manager', 'admin'),
        timeInputController.createBulkEntries);

router
    .route('/')
    .get(authController.protect, timeInputController.getAllTimeEntries)
    .post(
        authController.protect,
        authController.restrictTo('manager', 'admin'),
        timeInputController.createTimeEntry
    ).patch(authController.protect,
            authController.restrictTo('admin'),
            timeInputController.updateTimeEntry);

router
    .route('/uuids/')
    .post(authController.protect, authController.restrictTo('admin'), timeInputController.getTimeEntriesByIds);

module.exports = router;