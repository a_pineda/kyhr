const express = require('express');
const taskController = require('./../controllers/taskController');
const authController = require('./../controllers/authController');

const router = express.Router();

router
    .route('/')
    .get(authController.protect, taskController.getAllTasks)
    .post(
        authController.protect,
        authController.restrictTo('manager', 'admin'),
        taskController.createTask
    );

router
    .route('/company/:id')
    .get( authController.protect, taskController.getAllTasksForCompany);

router.patch('/updateTask', authController.protect, authController.restrictTo('manager', 'admin'), taskController.updateTask);

module.exports = router;