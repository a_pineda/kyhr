const express = require('express');
const nodemailController = require('./../controllers/nodemailController');
const authController = require('./../controllers/authController');

const router = express.Router();


router
    .route('/dailyReportSubmission')
    .post(
        authController.protect,
        authController.restrictTo('manager', 'admin'),
        nodemailController.sendDailyReportSubmittedEmail
    );

module.exports = router;