const express = require('express');
const companyController = require('./../controllers/companyController');
const authController = require('./../controllers/authController');

const router = express.Router();

router
    .route('/')
    .get(authController.protect, companyController.getAllCompanies)
    .post(
        authController.protect,
        authController.restrictTo('admin'),
        companyController.createCompany
    );

router
    .route('/:id')
    .get(authController.protect, companyController.getCompany)


router.patch('/updateCompany', authController.protect, authController.restrictTo('admin'), companyController.updateCompany);

module.exports = router;