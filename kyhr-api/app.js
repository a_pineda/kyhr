const express = require('express');
const morgan = require('morgan');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const path = require('path');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const addRequestId = require('express-request-id');

const AppError = require('./utils/appError');
const userRouter = require('./routes/userRoutes');
const payStatementRouter = require('./routes/payStatementRoutes');
const timeEntryRouter = require('./routes/timeInputRoutes');
const locationRouter = require('./routes/locationRoutes');
const taskRouter = require('./routes/taskRoutes');
const nodemailRouter = require('./routes/nodemailRoutes');
const companyRouter = require('./routes/companyRoutes');
const timeInputSubmissionRecordRouter = require('./routes/timeInputSubmissionRecordRoutes');
const globalErrorHandler = require('./controllers/errorController');

const app = express();

//==================================
// GLOBAL MIDDLEWARES
//==================================

//Static files
app.use(express.static(path.join(__dirname, "..", "kyhr-client", "app", "build")));

//Security headers
app.use(helmet());

app.use(cors());

app.use(addRequestId());

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

//Rate limiting
const limiter = rateLimit({
    max: 100,
    windowMs: 60 * 60 * 1000,
    message: 'Too many request from this IP, please try again in an hour.'
});

app.use('/api', limiter);

//Body parser
app.use(express.json({ limit: '10kb' }));

app.use(cookieParser());

//Test Middleware
app.use((req, res, next) => {
    req.requestTime = new Date().toISOString();
    next();
});

//==================================
// ROUTES
//==================================

app.use('/api/v1/users', userRouter);
app.use('/api/v1/paystatements', payStatementRouter);
app.use('/api/v1/timeentry', timeEntryRouter);
app.use('/api/v1/locations', locationRouter);
app.use('/api/v1/tasks', taskRouter);
app.use('/api/v1/mail', nodemailRouter);
app.use('/api/v1/company', companyRouter);
app.use('/api/v1/timeInputSubmissionRecords', timeInputSubmissionRecordRouter);


app.all('*', (req, res, next) => {
    res.redirect("/");
    //next(new AppError(`Cannot find ${req.originalUrl}`, 404));
    next();
});

app.use(globalErrorHandler);

module.exports = app;
