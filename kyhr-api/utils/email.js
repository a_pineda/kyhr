const nodemailer = require('nodemailer');
const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendEmail = async options => {

    //1) Define the email options

    const msg = {
        to: options.email,
        from: 'app.kyhr@gmail.com',
        subject: options.subject,
        text: options.message
    }

    //2) Send the email with sendgrid

    await sgMail.send(msg);

};

module.exports = sendEmail;
