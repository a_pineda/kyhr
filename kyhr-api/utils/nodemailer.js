const sgMail = require('@sendgrid/mail');
const fs = require('fs');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

// async..await is not allowed in global scope, must use a wrapper
async function sendDailyReportSubmittedEmail(csvFilepath, bodyText, receivers, date) {

    const msg = {
        to: receivers,
        from: {
            email: 'app.kyhr@gmail.com',
            name: 'KYHR'
        },
        subject: 'Daily Report Submission',
        text: 'Daily Report Submission',
        html: bodyText,
        attachments: [
            {
                content: fs.readFileSync(csvFilepath).toString('base64'),
                filename: "data.csv",
                type: "text/csv",
                disposition: "attachment"
            }
        ]
    };

    await sgMail.send(msg).catch((err) => {
        console.log(err);
    });
}

const csvToHtmlTable = (csv, date) => {
    const rows = csv.map((row) => {
        return `<tr>
                    <td style="text-align: center; border: 1px solid black">${row[0]}</td>
                    <td style="text-align: center; border: 1px solid black">${row[1]}</td>
                    <td style="text-align: center; border: 1px solid black">${row[2]}</td>
                    <td style="text-align: center; border: 1px solid black">${row[3]}</td>
                    <td style="text-align: center; border: 1px solid black">${row[4]}</td>
                </tr>`
    }).join('');
    return `<h2>Daily Report Submission</h2>
            <h4>For Date: ${date}</h4>
            <table style="border-collapse: collapse; border: 1px solid black">
                <tr>
                    <th style="text-align: center; border: 1px solid black">Name</th>
                    <th style="text-align: center; border: 1px solid black">Hours</th>
                    <th style="text-align: center; border: 1px solid black">Location</th>
                    <th style="text-align: center; border: 1px solid black">Task</th>
                    <th style="text-align: center; border: 1px solid black">Notes</th>
                </tr>
                ${rows}
            </table>`
}

module.exports = {sendDailyReportSubmittedEmail, csvToHtmlTable}